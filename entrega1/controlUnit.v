module controlUnit(
    input [5:0] inst,
    input [5:0] Opcode,
    output reg jump,
    output reg beqs,
    output reg regDest,
    output reg memRead,
    output reg memtoReg,
    output reg [2:0] aluOp,
    output reg memWrite,
    output reg aluSrc,
    output reg regWrite,
    output reg bnes
);



always @ (inst or Opcode) begin
    aluOp = 3'd3;
    regDest = 1'd1;
    aluSrc = 1'd0;
    memtoReg = 1'd0;
    regWrite = 1'd1;
    memRead = 1'd0;
    memWrite = 1'd0;
    jump = 1'd0;
    beqs = 1'd0;
    bnes = 1'd0;
    case(Opcode)
        6'h00: //! R-Type
            case(inst)
                6'h20: //! ADD
                    begin
                        aluOp = 3'd0;
                        regDest = 1'd1;
                        aluSrc = 1'd0;
                        memtoReg = 1'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
                    end
                6'h22: //! SUB
                    begin
                        aluOp = 3'd1;
                        regDest = 1'd1;
                        aluSrc = 1'd0;
                        memtoReg = 1'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
                    end
                6'h24: //! AND
                    begin
                        aluOp = 3'd2;
                        regDest = 1'd1;
                        aluSrc = 1'd0;
                        memtoReg = 1'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
                    end
                6'h25: //! OR
                    begin
                        aluOp = 3'd3;
                        regDest = 1'd1;
                        aluSrc = 1'd0;
                        memtoReg = 1'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
                    end
                6'h2A: //! SLT
                    begin
                        aluOp = 3'd4;
                        regDest = 1'd1;
                        aluSrc = 1'd0;
                        memtoReg = 1'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
                    end
            endcase
        6'h23: //! LW
        begin
            regDest = 1'd0;
            aluSrc = 1'd1;
            memtoReg = 1'd1;
            regWrite = 1'd1;
            memRead = 1'd1;
            memWrite = 1'd0;
        end
        6'h2B: //! SW
        begin
            regDest = 1'dx;
            aluSrc = 1'd1;
            memtoReg = 1'dx;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd1;
        end
        6'h04: //! BEQ
        begin
            regDest = 1'dx;
            aluSrc = 1'd0;
            memtoReg = 1'dx;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd0;
            beqs = 1'd1;
        end
        6'h05: //! BNE
        begin
            regDest = 1'dx;
            aluSrc = 1'd0;
            memtoReg = 1'dx;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd0;
            bnes = 1'd1;
        end
        6'h02: //! J
        begin
            regDest = 1'dx;
            aluSrc = 1'd0;
            memtoReg = 1'dx;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd0;
            jump = 1'd1;
        end
    endcase
end

endmodule
