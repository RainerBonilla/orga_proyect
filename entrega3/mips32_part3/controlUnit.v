module controlUnit(
    input [5:0] inst,
    input [5:0] Opcode,
    output reg jump,
    output reg beqs,
    output reg regDest,
    output reg memRead,
    output reg [1:0] memtoReg,
    output reg [2:0] aluOp,
    output reg memWrite,
    output reg aluSrc,
    output reg regWrite,
    output reg bnes,
    output reg sze,
    output reg [1:0]mds,
    output reg iop,
    output reg szel
);



always @ (inst or Opcode) begin
    aluOp = 3'dx;
    regDest = 1'd1;
    aluSrc = 1'd0;
    memtoReg = 2'd0;
    regWrite = 1'd1;
    memRead = 1'd0;
    memWrite = 1'd0;
    jump = 1'd0;
    beqs = 1'd0;
    bnes = 1'd0;
    sze = 1'd0;
    szel = 1'd1;
    mds = 2'd0;
    iop = 1'b1;
    case(Opcode)
        6'h00: //! R-Type
            case(inst)
                6'h20: //! ADD
                    begin
                        aluOp = 3'd0;
                        regDest = 1'd1;
                        aluSrc = 1'd0;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                    end
                6'h22: //! SUB
                    begin
                        aluOp = 3'd1;
                        regDest = 1'd1;
                        aluSrc = 1'd0;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                    end
                6'h24: //! AND
                    begin
                        aluOp = 3'd2;
                        regDest = 1'd1;
                        aluSrc = 1'd0;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                    end
                6'h25: //! OR
                    begin
                        aluOp = 3'd3;
                        regDest = 1'd1;
                        aluSrc = 1'd0;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                    end
                6'h2A: //! SLT
                    begin
                        aluOp = 3'd4;
                        regDest = 1'd1;
                        aluSrc = 1'd0;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                    end
		6'h21: //! ADDU
                    begin
                        aluOp = 3'd0;
                        regDest = 1'd1;
                        aluSrc = 1'd0;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                    end
            endcase
        6'h23: //! LW
        begin
	        aluOp = 3'd0;
            regDest = 1'd0;
            aluSrc = 1'd1;
            memtoReg = 2'd1;
            regWrite = 1'd1;
            memRead = 1'd1;
            memWrite = 1'd0;
	        iop = 1'b0;
        end
        6'h2B: //! SW
        begin
	    regDest = 1'd0;
            aluOp = 3'd0;
            aluSrc = 1'd1;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd1;
  	        iop = 1'b0;
        end
        6'h04: //! BEQ
        begin
            regDest = 1'd0;
            aluSrc = 1'd0;
            memtoReg = 2'd0;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd0;
            beqs = 1'd1;
	        aluOp = 3'd1;
            iop = 1'b0;
        end
        6'h05: //! BNE
        begin
	    regDest = 1'd0;
            aluSrc = 1'd0;
            memtoReg = 2'd0;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd0;
            bnes = 1'd1;
            aluOp = 3'd1;
            iop = 1'b0;
        end
        6'h02: //! J
        begin
            regDest = 1'd0;
            aluSrc = 1'd0;
            memtoReg = 2'd0;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd0;
            jump = 1'd1;
            iop = 1'b0;
        end
	6'h08: //! ADDI
        begin
            aluOp = 3'd0;
            regDest = 1'd0;
            aluSrc = 1'd1;
            memtoReg = 2'd0;
            regWrite = 1'd1;
            memRead = 1'd0;
            memWrite = 1'd0;
            iop = 1'b0;
        end
	6'h09: //! ADDIU
        begin
            aluOp = 3'd0;
            regDest = 1'd0;
            aluSrc = 1'd1;
            memtoReg = 2'd0;
            regWrite = 1'd1;
            memRead = 1'd0;
            memWrite = 1'd0;
            iop = 1'b0;
        end
	6'h0C: //! ANDI
        begin
            aluOp = 3'd2;
            regDest = 1'd0;
            aluSrc = 1'd1;
            memtoReg = 2'd0;
            regWrite = 1'd1;
            memRead = 1'd0;
            memWrite = 1'd0;
	        sze = 1'd1;
            iop = 1'b0;
        end
	6'h0D: //! ORI
        begin
            aluOp = 3'd3;
            regDest = 1'd0;
            aluSrc = 1'd1;
            memtoReg = 2'd0;
            regWrite = 1'd1;
            memRead = 1'd0;
            memWrite = 1'd0;
            sze = 1'd1;
            iop = 1'b0;
        end
	6'h0F: //! LUI
        begin
            regDest = 1'd0;
            memtoReg = 2'd2;
            memWrite = 1'd0;
	        regWrite = 1'd1;
	        memRead = 1'd0;
            iop = 1'b0;
        end
    6'h21: //! LH
        begin
            aluOp = 3'd0;
            regDest = 1'd0;
            aluSrc = 1'd1;
            memtoReg = 2'd1;
            regWrite = 1'd1;
            memRead = 1'd1;
            memWrite = 1'd0;
	        iop = 1'b0;
            mds = 2'd1;
        end
    6'h25: //! LHU
        begin
            aluOp = 3'd0;
            regDest = 1'd0;
            aluSrc = 1'd1;
            memtoReg = 2'd1;
            regWrite = 1'd1;
            memRead = 1'd1;
            memWrite = 1'd0;
	        iop = 1'b0;
            mds = 2'd1;
            szel = 1'b0;
        end
    6'h20: //! LB
        begin
            aluOp = 3'd0;
            regDest = 1'd0;
            aluSrc = 1'd1;
            memtoReg = 2'd1;
            regWrite = 1'd1;
            memRead = 1'd1;
            memWrite = 1'd0;
	        iop = 1'b0;
            mds = 2'd2;
        end
    6'h24: //! LBU
        begin
            aluOp = 3'd0;
            regDest = 1'd0;
            aluSrc = 1'd1;
            memtoReg = 2'd1;
            regWrite = 1'd1;
            memRead = 1'd1;
            memWrite = 1'd0;
	        iop = 1'b0;
            mds = 2'd2;
            szel = 1'b0;
        end
    6'h29: //! SH
        begin
	        regDest = 1'd0;
            aluOp = 3'd0;
            aluSrc = 1'd1;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd1;
  	        iop = 1'b0;
            mds = 2'd1;
        end
    6'h28: //! SB
        begin
	        regDest = 1'd0;
            aluOp = 3'd0;
            aluSrc = 1'd1;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd1;
  	        iop = 1'b0;
            mds = 2'd2;
        end
    endcase
end

endmodule