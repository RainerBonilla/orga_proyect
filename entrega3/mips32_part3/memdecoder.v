module Memorydecoder(
	input memr,
	input memw,
	input [31:0] virtualAdd,
	output reg [12:0] physicalAdd,
	output reg invalidAdd,
	output reg [2:0] me,
	output reg [1:0] mb
);

always @(virtualAdd)
begin
	if(virtualAdd >= 32'h10010000 && virtualAdd < 32'h10011000)begin
		physicalAdd = virtualAdd - 32'h10010000;
		invalidAdd = 1'b0;
		mb = 2'd0;
		me = (memr || memw) ? 3'b001 : 3'b000;
	end
	else if(virtualAdd >= 32'h7FFFEFFC && virtualAdd < 32'h7FFFFFFC)begin
		physicalAdd = virtualAdd - 32'hFFFEFFC + 13'h1000;
		invalidAdd = 1'b0;
		mb = 2'd0;
		me = (memr || memw) ? 3'b001 : 3'b000;
	end
	else if(virtualAdd >= 32'hB800 && virtualAdd <= 32'hCABF)begin
		physicalAdd = virtualAdd - 32'hB8000;
		invalidAdd = 1'b0;
		mb = 2'd1;
		me = (memw || memr) ? 3'b010 : 3'b000;
	end
	else if(virtualAdd >= 32'hFFFF0000 && virtualAdd <= 32'hFFFF000F)begin
		physicalAdd = virtualAdd;
		invalidAdd = 1'b0;
		mb = 2'd2;
		me = (memw || memr) ? 3'b100 : 3'b000;
	end
	else begin
		physicalAdd = 32'hx;
		invalidAdd = (memw || memr);
		mb = 2'dx;
		me = 3'b000;
	end
end

endmodule