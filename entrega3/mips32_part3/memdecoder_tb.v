//  A testbench for memdecoder_tb
`timescale 1us/1ns

module memdecoder_tb;
    reg [31:0] VAddr;
    wire [12:0] PAddr;
    wire IAd;

  memdecoder memdecoder0 (
    .VAddr(VAddr),
    .PAddr(PAddr),
    .IAd(IAd)
  );

    reg [45:0] patterns[0:21];
    integer i;

    initial begin
      patterns[0] = 46'b00010000000000010000000000000000_0000000000000_0;
      patterns[1] = 46'b00010000000000010000000000000001_0000000000001_0;
      patterns[2] = 46'b00010000000000010000000000000010_0000000000010_0;
      patterns[3] = 46'b00010000000000010000000000000011_0000000000011_0;
      patterns[4] = 46'b00010000000000010000000000000100_0000000000100_0;
      patterns[5] = 46'b00010000000000010000000000000101_0000000000101_0;
      patterns[6] = 46'b00010000000000010000000000000110_0000000000110_0;
      patterns[7] = 46'b00010000000000010000000000000111_0000000000111_0;
      patterns[8] = 46'b00010000000000010001000000000000_xxxxxxxxxxxxx_1;
      patterns[9] = 46'b00010000000000001111111111111111_xxxxxxxxxxxxx_1;
      patterns[10] = 46'b00010000000000010000111111111111_0111111111111_0;
      patterns[11] = 46'b01111111111111111110111111111100_1000000000000_0;
      patterns[12] = 46'b01111111111111111110111111111101_1000000000001_0;
      patterns[13] = 46'b01111111111111111110111111111110_1000000000010_0;
      patterns[14] = 46'b01111111111111111110111111111111_1000000000011_0;
      patterns[15] = 46'b01111111111111111111000000000000_1000000000100_0;
      patterns[16] = 46'b01111111111111111111000000000001_1000000000101_0;
      patterns[17] = 46'b01111111111111111111000000000010_1000000000110_0;
      patterns[18] = 46'b01111111111111111111000000000011_1000000000111_0;
      patterns[19] = 46'b01111111111111111111111111111100_xxxxxxxxxxxxx_1;
      patterns[20] = 46'b01111111111111111110111111111011_xxxxxxxxxxxxx_1;
      patterns[21] = 46'b01111111111111111111111111111011_1111111111111_0;

      for (i = 0; i < 22; i = i + 1)
      begin
        VAddr = patterns[i][45:14];
        #10;
        if (patterns[i][13:1] !== 13'hx)
        begin
          if (PAddr !== patterns[i][13:1])
          begin
            $display("%d:PAddr: (assertion error). Expected %h, found %h", i, patterns[i][13:1], PAddr);
            $finish;
          end
        end
        if (patterns[i][0] !== 1'hx)
        begin
          if (IAd !== patterns[i][0])
          begin
            $display("%d:IAd: (assertion error). Expected %h, found %h", i, patterns[i][0], IAd);
            $finish;
          end
        end
      end

      $display("All tests passed.");
    end
    endmodule
