//  A testbench for MIPS32CPU_testb_tb
`timescale 1us/1ns

module MIPS32CPU_testb_tb;
    reg clk;
    reg rst;
    wire [31:0] \$t0 ;
    wire [31:0] \$t1 ;
    wire [31:0] \($a3) ;
    wire [31:0] \$t2 ;
    wire [31:0] instruc;

  MIPS32CPU MIPS32CPU0 (
    .clk(clk),
    .rst(rst),
    .\$t0 (\$t0 ),
    .\$t1 (\$t1 ),
    .\($a3) (\($a3) ),
    .\$t2 (\$t2 ),
    .instruc(instruc)
  );

    reg [129:0] patterns[0:20];
    integer i;

    initial begin
      patterns[0] = 130'b0_1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[1] = 130'b1_1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[2] = 130'b0_1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_00000000000000000000000000001010_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[3] = 130'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[4] = 130'b1_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[5] = 130'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_00000000000000000000000000001010_00000000000000000000000000001111_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[6] = 130'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[7] = 130'b1_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[8] = 130'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_00000000000000000000000000001010_00000000000000000000000000001111_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[9] = 130'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[10] = 130'b1_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[11] = 130'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_00000000000000000000000000001010_00000000000000000000000000001111_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[12] = 130'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[13] = 130'b1_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[14] = 130'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_00000000000000000000000000001111_00000000000000000000000000001111_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[15] = 130'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[16] = 130'b1_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[17] = 130'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_00000000000000000000000000001111_00000000000000000000000000001010_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[18] = 130'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[19] = 130'b1_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[20] = 130'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_00000000000000000000000000001111_00000000000000000000000000001010_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;

      for (i = 0; i < 21; i = i + 1)
      begin
        clk = patterns[i][129];
        rst = patterns[i][128];
        #10;
        if (patterns[i][127:96] !== 32'hx)
        begin
          if (instruc !== patterns[i][127:96])
          begin
            $display("%d:instruc: (assertion error). Expected %h, found %h", i, patterns[i][127:96], instruc);
            $finish;
          end
        end
        if (patterns[i][95:64] !== 32'hx)
        begin
          if (\$t0  !== patterns[i][95:64])
          begin
            $display("%d:\$t0 : (assertion error). Expected %h, found %h", i, patterns[i][95:64], \$t0 );
            $finish;
          end
        end
        if (patterns[i][63:32] !== 32'hx)
        begin
          if (\$t1  !== patterns[i][63:32])
          begin
            $display("%d:\$t1 : (assertion error). Expected %h, found %h", i, patterns[i][63:32], \$t1 );
            $finish;
          end
        end
        if (patterns[i][31:0] !== 32'hx)
        begin
          if (\($a3)  !== patterns[i][31:0])
          begin
            $display("%d:\($a3) : (assertion error). Expected %h, found %h", i, patterns[i][31:0], \($a3) );
            $finish;
          end
        end
      end

      $display("All tests passed.");
    end
    endmodule
