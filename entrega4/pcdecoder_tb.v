//  A testbench for pcdecoder_tb
`timescale 1us/1ns

module pcdecoder_tb;
    reg [31:0] in;
    wire [12:0] out;
    wire inva;

  pcdecoder pcdecoder0 (
    .in(in),
    .out(out),
    .inva(inva)
  );

    reg [45:0] patterns[0:5];
    integer i;

    initial begin
      patterns[0] = 46'b00000000010000000000000000000000_0000000000000_0;
      patterns[1] = 46'b00000000010000000000000000000001_0000000000001_0;
      patterns[2] = 46'b00000000010000000000000000000100_0000000000100_0;
      patterns[3] = 46'b00000000010100000000000000000000_xxxxxxxxxxxxx_1;
      patterns[4] = 46'b00000000010000000001000000000000_xxxxxxxxxxxxx_1;
      patterns[5] = 46'b00000000000000000000000000000000_xxxxxxxxxxxxx_1;

      for (i = 0; i < 6; i = i + 1)
      begin
        in = patterns[i][45:14];
        #10;
        if (patterns[i][13:1] !== 13'hx)
        begin
          if (out !== patterns[i][13:1])
          begin
            $display("%d:out: (assertion error). Expected %h, found %h", i, patterns[i][13:1], out);
            $finish;
          end
        end
        if (patterns[i][0] !== 1'hx)
        begin
          if (inva !== patterns[i][0])
          begin
            $display("%d:inva: (assertion error). Expected %h, found %h", i, patterns[i][0], inva);
            $finish;
          end
        end
      end

      $display("All tests passed.");
    end
    endmodule
