module MemWrEn(
	input [31:0] indata,
	input [1:0]off,
	input we,
	input [1:0] dtype,
	output reg [31:0]outdata,
	output reg [3:0]outwe
);

always @(*) begin
	if(we) begin
        outdata = 32'd0;
        outwe = 4'b0000;
		case(dtype)
			2'd0:begin
				outdata = indata;
                outwe = 4'b1111;
            end
			2'd1: begin
				if(off <= 2'd1) begin
					outdata =  {indata[15:0], {16{1'b0}}};
					outwe = 4'b0011;
				end
				else begin 
					outdata = {{16{1'b0}}, indata[15:0]};
					outwe = 4'b1100;
				end
			end
			2'd2: begin
				case(off)
					2'd0: begin
						outdata = {indata[7:0], {24{1'b0}}};
						outwe = 4'b0001; 
					end
					2'd1: begin
						outdata = {{8{1'b0}}, indata[7:0], {16{1'b0}}};
						outwe = 4'b0010;
					end
					2'd2: begin
						outdata = {{16{1'b0}}, indata[7:0], {8{1'b0}}};
						outwe = 4'b0100;
					end
					2'd3: begin
						outdata = {{24{1'b0}}, indata[7:0]};
						outwe = 4'b1000;
					end
				endcase
			end
		endcase
	end
    else begin
        outdata = 32'd0;
        outwe = 4'b0000;
    end
end

endmodule