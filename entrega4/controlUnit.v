module controlUnit(
    input [5:0] inst,
    input [5:0] Opcode,
    input [4:0] branch, //! BGEZ/BLTZ
    output reg jump,
    output reg beqs,
    output reg [1:0] regDest,
    output reg memRead,
    output reg [1:0] memtoReg,
    output reg [3:0] aluOp,
    output reg memWrite,
    output reg [1:0] aluSrc,
    output reg regWrite,
    output reg bnes,
    output reg sze,
    output reg [1:0]mds,
    output reg iop,
    output reg szel,
    output reg jr,
    output reg jal,
    output reg blez,
    output reg bltz,
    output reg bgez,
    output reg bgtz,
    output reg shift
);



always @ (inst or Opcode) begin
    aluOp = 4'dx;
    regDest = 2'd0;
    aluSrc = 2'd0;
    memtoReg = 2'd0;
    regWrite = 1'd1;
    memRead = 1'd0;
    memWrite = 1'd0;
    jump = 1'd0;
    beqs = 1'd0;
    bnes = 1'd0;
    sze = 1'd0;
    szel = 1'd1;
    mds = 2'd0;
    iop = 1'b1;
    jr = 1'b0;
    jal = 1'b0;
    blez = 1'b0;
    bltz = 1'b0;
    bgez = 1'b0;
    bgtz = 1'b0;
    shift = 1'b0;
    case(Opcode)
        6'h00: //! R-Type
            case(inst)
                6'h20: //! ADD
                    begin
                        aluOp = 4'd0;
                        regDest = 2'd1;
                        aluSrc = 2'd0;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                    end
                6'h22: //! SUB
                    begin
                        aluOp = 4'd1;
                        regDest = 2'd1;
                        aluSrc = 2'd0;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                    end
                6'h24: //! AND
                    begin
                        aluOp = 4'd2;
                        regDest = 2'd1;
                        aluSrc = 2'd0;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                    end
                6'h25: //! OR
                    begin
                        aluOp = 4'd3;
                        regDest = 2'd1;
                        aluSrc = 2'd0;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                    end
                6'h2A: //! SLT
                    begin
                        aluOp = 4'd4;
                        regDest = 2'd1;
                        aluSrc = 2'd0;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                    end
	        	6'h21: //! ADDU
                    begin
                        aluOp = 4'd0;
                        regDest = 2'd1;
                        aluSrc = 2'd0;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                    end
                6'h2B: //! SLTU
                    begin
                        aluOp = 4'd6;
                        regDest = 2'd1;
                        aluSrc = 2'd0;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                    end
                6'h26: //! XOR
                    begin
                        aluOp = 4'd5;
                        regDest = 2'd1;
                        aluSrc = 2'd0;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                    end
                6'h00: //! SLL
                    begin
                        aluOp = 4'd7;
                        regDest = 2'd1;
                        aluSrc = 2'd2;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                        shift = 1'b1;
                    end
                6'h04: //! SLLV
                    begin
                        aluOp = 4'd7;
                        regDest = 2'd1;
                        aluSrc = 2'd3;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                        shift = 1'b1;
                    end
                6'h02: //! SRL
                    begin
                        aluOp = 4'd8;
                        regDest = 2'd1;
                        aluSrc = 2'd2;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                        shift = 1'b1;
                    end
                6'h06: //! SRLV
                    begin
                        aluOp = 4'd8;
                        regDest = 2'd1;
                        aluSrc = 2'd3;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                        shift = 1'b1;
                    end
                6'h03: //! SRA
                    begin
                        aluOp = 4'd9;
                        regDest = 2'd1;
                        aluSrc = 2'd2;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                        shift = 1'b1;
                    end
                6'h07: //! SRAV
                    begin
                        aluOp = 4'd9;
                        regDest = 2'd1;
                        aluSrc = 2'd3;
                        memtoReg = 2'd0;
                        regWrite = 1'd1;
                        memRead = 1'd0;
                        memWrite = 1'd0;
		            	iop = 1'b0;
                        shift = 1'b1;
                    end
                6'h08: //! JR
                    begin
                        regDest = 2'd0;
                        aluSrc = 2'd0;
                        memtoReg = 2'd0;
                        regWrite = 1'd0;
                        memRead = 1'd0;
                        memWrite = 1'd0;
                        jr = 1'd1;
                        iop = 1'b0;
                    end
            endcase
        6'h23: //! LW
        begin
	        aluOp = 4'd0;
            regDest = 2'd0;
            aluSrc = 2'd1;
            memtoReg = 2'd1;
            regWrite = 1'd1;
            memRead = 1'd1;
            memWrite = 1'd0;
	        iop = 1'b0;
        end
        6'h2B: //! SW
        begin
	    regDest = 2'd0;
            aluOp = 4'd0;
            aluSrc = 2'd1;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd1;
  	        iop = 1'b0;
        end
        6'h04: //! BEQ
        begin
            regDest = 2'd0;
            aluSrc = 2'd0;
            memtoReg = 2'd0;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd0;
            beqs = 1'd1;
	        aluOp = 4'd1;
            iop = 1'b0;
        end
        6'h05: //! BNE
        begin
	    regDest = 2'd0;
            aluSrc = 2'd0;
            memtoReg = 2'd0;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd0;
            bnes = 1'd1;
            aluOp = 4'd1;
            iop = 1'b0;
        end
        6'h02: //! J
        begin
            regDest = 2'd0;
            aluSrc = 2'd0;
            memtoReg = 2'd0;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd0;
            jump = 1'd1;
            iop = 1'b0;
        end
	6'h08: //! ADDI
        begin
            aluOp = 4'd0;
            regDest = 2'd0;
            aluSrc = 2'd1;
            memtoReg = 2'd0;
            regWrite = 1'd1;
            memRead = 1'd0;
            memWrite = 1'd0;
            iop = 1'b0;
        end
	6'h09: //! ADDIU
        begin
            aluOp = 4'd0;
            regDest = 2'd0;
            aluSrc = 2'd1;
            memtoReg = 2'd0;
            regWrite = 1'd1;
            memRead = 1'd0;
            memWrite = 1'd0;
            iop = 1'b0;
        end
	6'h0C: //! ANDI
        begin
            aluOp = 4'd2;
            regDest = 2'd0;
            aluSrc = 2'd1;
            memtoReg = 2'd0;
            regWrite = 1'd1;
            memRead = 1'd0;
            memWrite = 1'd0;
	        sze = 1'd1;
            iop = 1'b0;
        end
	6'h0D: //! ORI
        begin
            aluOp = 4'd3;
            regDest = 2'd0;
            aluSrc = 2'd1;
            memtoReg = 2'd0;
            regWrite = 1'd1;
            memRead = 1'd0;
            memWrite = 1'd0;
            sze = 1'd1;
            iop = 1'b0;
        end
	6'h0F: //! LUI
        begin
            regDest = 2'd0;
            memtoReg = 2'd2;
            memWrite = 1'd0;
	        regWrite = 1'd1;
	        memRead = 1'd0;
            iop = 1'b0;
        end
    6'h21: //! LH
        begin
            aluOp = 4'd0;
            regDest = 2'd0;
            aluSrc = 2'd1;
            memtoReg = 2'd1;
            regWrite = 1'd1;
            memRead = 1'd1;
            memWrite = 1'd0;
	        iop = 1'b0;
            mds = 2'd1;
        end
    6'h25: //! LHU
        begin
            aluOp = 4'd0;
            regDest = 2'd0;
            aluSrc = 2'd1;
            memtoReg = 2'd1;
            regWrite = 1'd1;
            memRead = 1'd1;
            memWrite = 1'd0;
	        iop = 1'b0;
            mds = 2'd1;
            szel = 1'b0;
        end
    6'h20: //! LB
        begin
            aluOp = 4'd0;
            regDest = 2'd0;
            aluSrc = 2'd1;
            memtoReg = 2'd1;
            regWrite = 1'd1;
            memRead = 1'd1;
            memWrite = 1'd0;
	        iop = 1'b0;
            mds = 2'd2;
        end
    6'h24: //! LBU
        begin
            aluOp = 4'd0;
            regDest = 2'd0;
            aluSrc = 2'd1;
            memtoReg = 2'd1;
            regWrite = 1'd1;
            memRead = 1'd1;
            memWrite = 1'd0;
	        iop = 1'b0;
            mds = 2'd2;
            szel = 1'b0;
        end
    6'h29: //! SH
        begin
	        regDest = 2'd0;
            aluOp = 4'd0;
            aluSrc = 2'd1;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd1;
  	        iop = 1'b0;
            mds = 2'd1;
        end
    6'h28: //! SB
        begin
	        regDest = 2'd0;
            aluOp = 4'd0;
            aluSrc = 2'd1;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd1;
  	        iop = 1'b0;
            mds = 2'd2;
        end
    6'h0E: //! XORI
        begin
            aluOp = 4'd5;
            regDest = 2'd0;
            aluSrc = 2'd1;
            memtoReg = 2'd0;
            regWrite = 1'd1;
            memRead = 1'd0;
            memWrite = 1'd0;
            sze = 1'd1;
            iop = 1'b0;
        end
    6'h0A: //! SLTI
        begin
            regDest = 2'd1;
            aluSrc = 2'd1;
            memtoReg = 2'd0;
            regWrite = 1'd1;
            memRead = 1'd0;
            memWrite = 1'd0;
            iop = 1'b0;
            aluOp = 4'd4;
        end
    6'h0B: //! SLTIU
        begin
            regDest = 2'd1;
            aluSrc = 2'd1;
            memtoReg = 2'd0;
            regWrite = 1'd1;
            memRead = 1'd0;
            memWrite = 1'd0;
            iop = 1'b0;
            aluOp = 4'd6;
        end
    6'h03: //! JAL
        begin
            regDest = 2'd2;
            aluSrc = 2'd0;
            memtoReg = 2'd3;
            regWrite = 1'd1;
            memRead = 1'd0;
            memWrite = 1'd0;
            jal = 1'd1;
            iop = 1'b0;
        end
    6'h01: //! BGEZ or BLTZ
        begin
            if(branch == 5'd1)begin
                bgez = 1'd1;
                bltz = 1'd0;
            end
            else begin
                bgez = 1'd0;
                bltz = 1'd1;
            end
            aluOp = 4'd1;
            regDest = 2'd0;
            aluSrc = 2'd0;
            memtoReg = 2'd0;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd0;
            iop = 1'b0;
        end
    6'h06: //! BLEZ
        begin
            aluOp = 4'd1;
            regDest = 2'd0;
            aluSrc = 2'd0;
            memtoReg = 2'd0;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd0;
            iop = 1'b0;
            blez = 1'd1;
        end
    6'h07: //! BGTZ
        begin
            aluOp = 4'd1;
            regDest = 2'd0;
            aluSrc = 2'd0;
            memtoReg = 2'd0;
            regWrite = 1'd0;
            memRead = 1'd0;
            memWrite = 1'd0;
            iop = 1'b0;
            bgtz = 1'd1;
        end
    endcase
end

endmodule