module branchResolver(
	input beq,
	input bne,
	input bgez,
	input bgtz,
	input blez,
	input bltz,
	input zero,
	input sign,
	output reg bt
);

always @(*) begin
	if(beq)begin 
        bt = zero ? 1'b1 : 1'b0;
	end
	else if(bne)begin
		bt = zero ? 1'b0 : 1'b1;
	end
	else if(bgez)begin
		bt = sign ? 1'b0 : 1'b1;
	end
	else if(bgtz)begin
		bt = (~zero && ~sign) ? 1'b1 : 1'b0;
	end
	else if(blez)begin
		bt = (zero || sign) ? 1'b1 : 1'b0;
	end
	else if(bltz)begin
		bt = sign ? 1'b1 : 1'b0;
	end
	else begin 
		bt = 1'b0;
	end
end

endmodule
