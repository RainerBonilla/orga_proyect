module RAMV (
  input [7:0] A,
  input [31:0] Din,
  input str,
  input C,
  input Id,
  output [31:0] memo,
  output [7:0] Addr,
  output [31:0] D
);
reg [31:0] memory[0:((1 << 8) - 1)];

assign D = Id? memory[A] : 'hz;
assign memo = memory[A];
assign Addr = A;

always @ (posedge C) begin
   if (str)
     memory[A] <= Din;
end


endmodule
