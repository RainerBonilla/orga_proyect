/*
 * Generated by Digital. Don't modify this file!
 * Any changes will be lost if this file is regenerated.
 */
module Memorydecoder(
	input [31:0] virtualAdd,
	output reg [12:0] physicalAdd,
	output reg invalidAdd
);

always @(*)
begin
	if(virtualAdd >= 32'h10010000 && virtualAdd < 32'h10011000)begin
		physicalAdd = virtualAdd - 32'h10010000;
		invalidAdd = 1'b0;
	end
	else if(virtualAdd >= 32'h7FFFEFFC && virtualAdd < 32'h7FFFFFFC)begin
		physicalAdd = virtualAdd - 32'hFFFEFFC + 13'h1000;
		invalidAdd = 1'b0;
	end
	else begin
		physicalAdd = 32'hx;
		invalidAdd = 1'b1;
	end
end

endmodule

module memdecoder (
  input [31:0] VAddr,
  output [12:0] PAddr,
  output IAd
);
  // Memorydecoder
  Memorydecoder Memorydecoder_i0 (
    .virtualAdd( VAddr ),
    .physicalAdd( PAddr ),
    .invalidAdd( IAd )
  );
endmodule
