//  A testbench for MIPS32CPU_lui_tb
`timescale 1us/1ns

module MIPS32CPU_lui_tb;
    reg clk;
    reg rst;
    wire [31:0] t0;
    wire [31:0] t1;
    wire [31:0] t2;

  MIPS32CPU MIPS32CPU0 (
    .clk(clk),
    .rst(rst),
    .t0(t0),
    .t1(t1),
    .t2(t2)
  );

    reg [97:0] patterns[0:11];
    integer i;

    initial begin
      patterns[0] = 98'b0_1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[1] = 98'b1_1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[2] = 98'b0_1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[3] = 98'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[4] = 98'b1_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[5] = 98'b0_0_10101010101110110000000000000000_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[6] = 98'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[7] = 98'b1_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[8] = 98'b0_0_10101010101110110000000000000000_11001100110111010000000000000000_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[9] = 98'b0_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[10] = 98'b1_0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx;
      patterns[11] = 98'b0_0_10101010101110110000000000000000_11001100110111010000000000000000_00010001001000100000000000000000;

      for (i = 0; i < 12; i = i + 1)
      begin
        clk = patterns[i][97];
        rst = patterns[i][96];
        #10;
        if (patterns[i][95:64] !== 32'hx)
        begin
          if (t0 !== patterns[i][95:64])
          begin
            $display("%d:t0: (assertion error). Expected %h, found %h", i, patterns[i][95:64], t0);
            $finish;
          end
        end
        if (patterns[i][63:32] !== 32'hx)
        begin
          if (t1 !== patterns[i][63:32])
          begin
            $display("%d:t1: (assertion error). Expected %h, found %h", i, patterns[i][63:32], t1);
            $finish;
          end
        end
        if (patterns[i][31:0] !== 32'hx)
        begin
          if (t2 !== patterns[i][31:0])
          begin
            $display("%d:t2: (assertion error). Expected %h, found %h", i, patterns[i][31:0], t2);
            $finish;
          end
        end
      end

      $display("All tests passed.");
    end
    endmodule
