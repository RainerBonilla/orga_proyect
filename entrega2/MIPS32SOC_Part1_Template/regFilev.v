module REGFILEV (
  input [31:0] Din,
  input we,
  input [4:0] Rw,
  input C,
  input [4:0] Ra,
  input [4:0] Rb,
  output [31:0] Da,
  output [31:0] Db,
  output [31:0] t0,
  output [31:0] t1
);

reg [31:0] memory[0:((1 << 5)-1)];

initial begin
	memory[4] = 32'd15;
	memory[5] = 32'd10;
	memory[6] = 32'd2000;
end

assign Da = memory[Ra];
assign Db = memory[Rb];

assign t0 = memory[8];
assign t1 = memory[9];

always @ (posedge C) begin
     if (we)
        memory[Rw] <= Din;
end
endmodule
