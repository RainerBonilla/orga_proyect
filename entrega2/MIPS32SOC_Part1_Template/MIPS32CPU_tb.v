//  A testbench for MIPS32CPU_tb
`timescale 1us/1ns

module MIPS32CPU_tb;
    reg clk;
    wire [31:0] \$t0 ;
    wire [31:0] \$t1 ;
    wire [31:0] \($a3) ;
    wire [7:0] Adr;
    wire [31:0] instruc;
    wire [5:0] op;

  MIPS32CPU MIPS32CPU0 (
    .clk(clk),
    .\$t0 (\$t0 ),
    .\$t1 (\$t1 ),
    .\($a3) (\($a3) ),
    .Adr(Adr),
    .instruc(instruc),
    .op(op)
  );

    reg [142:0] patterns[0:39];
    integer i;

    initial begin
      patterns[0] = 143'b0_00000000101001000100000000101010_000000_00000000000000000000000000000000_00000000000000000000000000000000_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[1] = 143'b0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[2] = 143'b1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[3] = 143'b0_00010101000000000000000000000100_000101_00000000000000000000000000000001_00000000000000000000000000000000_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[4] = 143'b0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[5] = 143'b1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[6] = 143'b0_00000000100001010100000000100000_000000_00000000000000000000000000000001_00000000000000000000000000000000_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[7] = 143'b0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[8] = 143'b1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[9] = 143'b0_00000000100001010100100000100010_000000_00000000000000000000000000011001_00000000000000000000000000000000_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[10] = 143'b0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[11] = 143'b1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[12] = 143'b0_00000000100001010100000000101010_000000_00000000000000000000000000011001_00000000000000000000000000000101_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[13] = 143'b0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[14] = 143'b1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[15] = 143'b0_00010001000000001111111111111001_000100_00000000000000000000000000000000_00000000000000000000000000000101_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[16] = 143'b0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[17] = 143'b1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[18] = 143'b0_00000000100001010100000000100100_000000_00000000000000000000000000000000_00000000000000000000000000000101_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[19] = 143'b0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[20] = 143'b1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[21] = 143'b0_00000000100001010100100000100101_000000_00000000000000000000000000001010_00000000000000000000000000000101_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[22] = 143'b0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[23] = 143'b1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[24] = 143'b0_00001000000000000000000000001011_000010_00000000000000000000000000001010_00000000000000000000000000001111_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[25] = 143'b0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[26] = 143'b1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[27] = 143'b0_10101100111010000000000000000000_101011_00000000000000000000000000001010_00000000000000000000000000001111_00000000000000000000000000000000_11110100;
      patterns[28] = 143'b0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[29] = 143'b1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[30] = 143'b0_10101100111010010000000000000100_101011_00000000000000000000000000001010_00000000000000000000000000001111_00000000000000000000000000000000_11110101;
      patterns[31] = 143'b0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[32] = 143'b1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[33] = 143'b0_10001100111010000000000000000100_100011_00000000000000000000000000001010_00000000000000000000000000001111_00000000000000000000000000001111_11110101;
      patterns[34] = 143'b0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[35] = 143'b1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[36] = 143'b0_10001100111010010000000000000000_100011_00000000000000000000000000001111_00000000000000000000000000001111_00000000000000000000000000001010_11110100;
      patterns[37] = 143'b0_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[38] = 143'b1_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;
      patterns[39] = 143'b0_00000000000000000000000000000000_000000_00000000000000000000000000001111_00000000000000000000000000001010_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_xxxxxxxx;

      for (i = 0; i < 40; i = i + 1)
      begin
        clk = patterns[i][142];
        #10;
        if (patterns[i][141:110] !== 32'hx)
        begin
          if (instruc !== patterns[i][141:110])
          begin
            $display("%d:instruc: (assertion error). Expected %h, found %h", i, patterns[i][141:110], instruc);
            $finish;
          end
        end
        if (patterns[i][109:104] !== 6'hx)
        begin
          if (op !== patterns[i][109:104])
          begin
            $display("%d:op: (assertion error). Expected %h, found %h", i, patterns[i][109:104], op);
            $finish;
          end
        end
        if (patterns[i][103:72] !== 32'hx)
        begin
          if (\$t0  !== patterns[i][103:72])
          begin
            $display("%d:\$t0 : (assertion error). Expected %h, found %h", i, patterns[i][103:72], \$t0 );
            $finish;
          end
        end
        if (patterns[i][71:40] !== 32'hx)
        begin
          if (\$t1  !== patterns[i][71:40])
          begin
            $display("%d:\$t1 : (assertion error). Expected %h, found %h", i, patterns[i][71:40], \$t1 );
            $finish;
          end
        end
        if (patterns[i][39:8] !== 32'hx)
        begin
          if (\($a3)  !== patterns[i][39:8])
          begin
            $display("%d:\($a3) : (assertion error). Expected %h, found %h", i, patterns[i][39:8], \($a3) );
            $finish;
          end
        end
        if (patterns[i][7:0] !== 8'hx)
        begin
          if (Adr !== patterns[i][7:0])
          begin
            $display("%d:Adr: (assertion error). Expected %h, found %h", i, patterns[i][7:0], Adr);
            $finish;
          end
        end
      end

      $display("All tests passed.");
    end
    endmodule
