#ifndef _BMP_H_
#define _BMP_H_

#include <cstdint>

/*
bfType          must always be set to 'BM' to declare that this is a .bmp-file.
bfSize          specifies the size of the file in bytes.
bfReserved1     must always be set to zero.
bfReserved2     must always be set to zero.
bfOffBits       specifies the offset from the beginning of the file to the bitmap data.
*/
struct BITMAPFILEHEADER {
    uint16_t bfType;
    uint32_t bfSize;
    uint16_t bfRes1;
    uint16_t bfRes2;
    uint32_t bfOffset;
} __attribute__((packed));

/*
4	biSize	40	specifies the size of the BITMAPINFOHEADER structure, in bytes.
4	biWidth	100	specifies the width of the image, in pixels.
4	biHeight	100	specifies the height of the image, in pixels.
2	biPlanes	1	specifies the number of planes of the target device, must be set to zero.
2	biBitCount	8	specifies the number of bits per pixel.
4	biCompression	0	Specifies the type of compression, usually set to zero (no compression).
4	biSizeImage	0	specifies the size of the image data, in bytes. If there is no compression, it is valid to set this member to zero.
4	biXPelsPerMeter	0	specifies the the horizontal pixels per meter on the designated targer device, usually set to zero.
4	biYPelsPerMeter	0	specifies the the vertical pixels per meter on the designated targer device, usually set to zero.
4	biClrUsed	0	specifies the number of colors used in the bitmap, if set to zero the number of colors is calculated using the biBitCount member.
4	biClrImportant	0	specifies the number of color that are 'important' for the bitmap, if set to zero, all colors are important.
*/
struct BITMAPINFOHEADER {
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;	
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));

bool saveImageToBmp(const uint8_t *data, int w, int h, const char *filename);

#endif
