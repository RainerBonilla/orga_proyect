#include <cstdlib>
#include <cstdio>
#include <climits>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <memory>
#include <thread>
#include <limits>
#include "verilated.h"
#include "FL/Fl.H"
#include "VGATextWindow.h"
#include "Mips32SocSim.h"

std::string dirName (const std::string &path);

void usage(const char *progname) {
    std::cerr << "Usage : " << progname << " -i -s -sf <file name>\n"
              << "All arguments are optional. If no file name argument provided the output image file\n"
              << "will be named vga_display.bmp\n";
}

void my_callback(Fl_Widget*, void*) {
}

int main(int argc, char* argv[]) {
    char *progname = argv[0];
    argc--;
    argv++;

    std::string scrFile = "vga_display.bmp";
    bool printScr = false;
    bool initVGAFB = false;
    for (int i = 0; i < argc; i++) {
        if (strcmp(argv[i], "-s") == 0) {
            printScr = true;
        } else if (strcmp(argv[i], "-i") == 0) {
            initVGAFB = true;
        } else if (strcmp(argv[i], "-sf") == 0) {
            if (i < argc - 1) {
                scrFile = argv[i+1];
                i++;
            } else {
                usage(progname);
                return 1;
            }
        } else {
            usage(progname);
            return 1;
        }
    }

    VGATextWindow *vgatw = new VGATextWindow(640, 480);
    vgatw->callback(my_callback);
    vgatw->set_non_modal();
    Mips32Sim msim(vgatw);

    if (initVGAFB) {
        vgatw->initVGAContent();
    }

    std::thread simt (&Mips32Sim::run, &msim);

    Fl::lock();
    char *_argv[] = {progname};
    vgatw->show(1, _argv);
    int res = Fl::run();

    if (printScr) {
        vgatw->saveScreenshot(scrFile.c_str());
    }

    simt.join(); // Wait for the Simulator thread to finish

    return res;
}

std::string dirName(const std::string &path) {
    int lastSlashPos;

    lastSlashPos = path.find_last_of("/");

    if (lastSlashPos != std::string::npos) {
        return path.substr(0, lastSlashPos);
    } else {
        return "";
    }
}