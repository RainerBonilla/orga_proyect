#include <iostream>
#include <memory>
#include "VGATextWindow.h"

bool VGATextWindow::initDisplay(uint32_t *vgafb, uint8_t *vgarom, uint8_t *vgapal) {
    setVGAFrameBuffer(vgafb);
    loadVGAFont(vgarom);
    initColorPalette(vgapal);

    return true;
}

void VGATextWindow::initVGAContent() {
    uint16_t *p = reinterpret_cast<uint16_t *>(vgafb);

    uint16_t attr = 0xcf00;
    uint8_t ch = 0; 
    for (int r = 0; r < VGA_ROWS; r++) {
        for (int c = 0; c < VGA_COLS; c++) {
            *p = attr | (uint16_t)ch;
            p++;
            ch++;
        }
    }
}

void VGATextWindow::initColorPalette(const uint8_t *vgapal) {
    for (int i = 0; i < VGA_NUM_COLORS; i++) {
        uint8_t color = vgapal[i];

        uchar r = color & 0xe0;
        uchar g = (color & 0x1c) * 8;
        uchar b = (color & 0x03) * 64;
        pal[i] = fl_rgb_color(r, g, b);
    }
}

void VGATextWindow::loadVGAFont(const uint8_t *vgafont) {
    for (int i = 0; i < VGA_NUM_SYMBOLS; i++) {
        const uint8_t *symbData = &vgafont[i * CHAR_SIZE_BYTES];
        SymbPixels spix;
        loadVGAFontSymbol(symbData, spix);
        vgaSymbs.push_back(std::move(spix));
    }
}

void VGATextWindow::loadVGAFontSymbol(const uint8_t *symbData, SymbPixels& symbPixels) {
    double pixel_y = 0;

    for (int i = 0; i < CHAR_HEIGHT; i++) {
        uint8_t line_data = *symbData;
        uint8_t mask = 1 << ROW_MSB;

        double pixel_x = 0;
        for (int j = 0; j < CHAR_WIDTH; j++) {
            if ((line_data & mask) != 0) {
                symbPixels.emplace_back(pixel_x, pixel_y);
            }            
            pixel_x ++;
            mask >>= 1;
        }
        symbData += BYTES_PER_ROW;
        pixel_y ++;
    }    
}

void VGATextWindow::drawVGAContent() {
    if (vgaSymbs.size() == 0 || vgafb == nullptr) {
        return;
    }

    uint32_t *pvga = vgafb;
    
    for (int row = 0; row < VGA_ROWS; row++) {
        int col = 0;

        while (col < 80) {
            drawVGASymbol(row, col, *pvga >> 16);
            col++;
            drawVGASymbol(row, col, *pvga & 0xffff);
            col++;
            pvga++;
        }
    }
}

void VGATextWindow::drawChar(int x, int y, Fl_Color &fgcolor, Fl_Color &bgcolor, uint8_t chIndex) {
    const SymbPixels& spix = vgaSymbs[chIndex];

    fl_color(bgcolor);
    fl_rectf(x, y, CHAR_WIDTH, CHAR_HEIGHT);

    if (spix.size() > 0) {
        fl_color(fgcolor);
        fl_begin_points();
        for (const auto& pt : spix) {
            fl_vertex(x + pt.first, y + pt.second);
        }
        fl_end_points();
    }
}

void VGATextWindow::drawVGASymbol(int row, int col, uint16_t symb) {
    uint8_t chIndex = (uint8_t)(symb & 0xff);
    uint8_t fgindex = (symb & 0x0f00) >> 8;
    uint8_t bgindex = (symb & 0xf000) >> 12;

    Fl_Color fgcolor = getColorFromPalette(fgindex);
    Fl_Color bgcolor = getColorFromPalette(bgindex);

    drawChar(col * CHAR_WIDTH, row * CHAR_HEIGHT, fgcolor, bgcolor, chIndex);
}

void VGATextWindow::draw() {
    drawVGAContent();
}

int VGATextWindow::handle(int e) {
    int ret = Fl_Window::handle(e);
    switch(e) {
        case FL_KEYDOWN:
            switch (Fl::event_key()) {
                case FL_Left: kpstate &= ~(1 << 0); break;
                case FL_Right: kpstate &= ~(1 << 1); break;
                case FL_Down: kpstate &= ~(1 << 2); break;
                case FL_Up: kpstate &= ~(1 << 3); break;
                case 'q': kpstate &= ~(1 << 4); break;
                case 'p': kpstate &= ~(1 << 5); break;
                case 'b': kpstate &= ~(1 << 6); break;
                case ' ': kpstate &= ~(1 << 7); break;
                case 'r': rstKeyState = true; break;
                case 'x': stopKeyState = Fl::event_ctrl() != 0; break;
                default:
                    break;
            }
            break;
        case FL_KEYUP:
            switch (Fl::event_key()) {
                case FL_Left: kpstate |= (1 << 0); break;
                case FL_Right: kpstate |= (1 << 1); break;
                case FL_Down: kpstate |= (1 << 2); break;
                case FL_Up: kpstate |= (1 << 3); break;
                case 'q': kpstate |= (1 << 4); break;
                case 'p': kpstate |= (1 << 5); break;
                case 'b': kpstate |= (1 << 6); break;
                case ' ': kpstate |= (1 << 7); break;
                case 'r': rstKeyState = false; break;
                case 'x': stopKeyState = false; break;
                default:
                    break;
            }
            break;
    }

/*
    if (e == FL_KEYDOWN || e == FL_KEYUP) {
        if (rstKeyState) {
            std::cout << "RESET PRESSED\n" << std::flush;
            prevReset = true;
        } else if (prevReset) {
            prevReset = false;
            std::cout << "RESET RELEASED\n" << std::flush;
        }
    }
*/

    return ret;
}

void VGATextWindow::saveScreenshot(const char *filename) {
    int w = getWidth();
    int h = getHeight();

    Fl_Offscreen offscr = fl_create_offscreen(w, h);
    fl_begin_offscreen(offscr); /* Open the offscreen context for reading */
    std::vector<uchar> datav(w * h * 3);

    drawVGAContent();
    fl_read_image(datav.data(), 0, 0, w, h, 0); 
    fl_end_offscreen(); // close the offscreen context
    saveImageToBmp(datav.data(), w, h, filename);
}
