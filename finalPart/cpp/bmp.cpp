#include <cstring>
#include <fstream>
#include <iostream>
#include <memory>
#include "bmp.h"

bool saveImageToBmp(const uint8_t *data, int w, int h, const char *filename) {
    std::ofstream out(filename, std::ios::out | std::ios::trunc);

    if (!out.is_open()) {
        std::cout << "Cannot open output file '" << filename << "\n";
        return true;
    }

    BITMAPFILEHEADER bmpFH;
    bmpFH.bfType = 0x4d42; // BM
    bmpFH.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + (w * h * 3);
    bmpFH.bfRes1 = 0;
    bmpFH.bfRes2 = 0;
    bmpFH.bfOffset = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
    
    out.write(reinterpret_cast<char *>(&bmpFH), sizeof(BITMAPFILEHEADER));

    BITMAPINFOHEADER bmpIH;
    bmpIH.biSize = sizeof(BITMAPINFOHEADER);
    bmpIH.biWidth = w;
    bmpIH.biHeight = h;
    bmpIH.biPlanes = 1;
    bmpIH.biBitCount = 24;
    bmpIH.biCompression = 0;	
    bmpIH.biSizeImage = w * h * 3;
    bmpIH.biXPelsPerMeter = 3780;
    bmpIH.biYPelsPerMeter = 3780;
    bmpIH.biClrUsed = 0;
    bmpIH.biClrImportant = 0;

    out.write(reinterpret_cast<char *>(&bmpIH), sizeof(BITMAPINFOHEADER));
    
    // The BITMAP rows are stored upside down
    int lastRow = (h - 1) * w * 3;
    int rowSizeBytes = w * 3;
    int paddedRowSizeBytes = ((rowSizeBytes + 3)/4) * 4; // 4 byte aligned
    std::unique_ptr<char[]> rowBuff(new char[paddedRowSizeBytes]);
    char *rowPtr = rowBuff.get();
    const uint8_t *dataPtr = &data[lastRow];

    for (int y = 0; y < h; y ++) {
        memset(rowPtr, 0, paddedRowSizeBytes);

        // BITMAP file uses BGR instead of RGB
        char *dst = rowPtr;
        const uint8_t *src  = dataPtr;
        for (int x = 0; x < w; x++) {
            uint8_t r = *src++;
            uint8_t g = *src++;
            uint8_t b = *src++;

            *dst++ = b;
            *dst++ = g;
            *dst++ = r;
        }
        out.write(rowPtr, paddedRowSizeBytes);
        dataPtr -= rowSizeBytes;
    }
   out.close();
   return true;
}
