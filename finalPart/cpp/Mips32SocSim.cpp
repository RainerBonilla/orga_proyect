/*
 * MIPS32Sim.cpp
 *
 *  Created on: Jul 5, 2017
 *      Author: ideras
 */
#include <iostream>
#include <iomanip>
#include <string>
#include <chrono>
#include <FL/Fl.H>
#include "VMips32soc.h"
#include "VMips32soc_Mips32soc.h"
#include "VMips32soc_VGATextCard.h"
#include "VMips32soc_FontRom.h"
#include "VMips32soc_DualPortVGARam.h"
#include "VMips32soc_RomColorPalette.h"
#include "VMips32soc_RegFile.h"
#include "VMips32soc_DataMem.h"
#include "VMips32soc_InstMem.h"
#include "VGATextWindow.h"
#include "Mips32SocSim.h"

const char *reg_names[] = {
    "$zero", "$at", "$v0", "$v1",
    "$a0", "$a1", "$a2", "$a3",
    "$t0", "$t1", "$t2", "$t3",
    "$t4", "$t5", "$t6", "$t7",
    "$s0", "$s1", "$s2", "$s3",
    "$s4", "$s5", "$s6", "$s7",
    "$t8", "$t9", "$k0", "$k1",
    "$gp", "$sp", "$fp", "$ra"
};

bool repaintFlag = false;

void repaintTimerCallback(void *data) {
    if (repaintFlag) {
        Mips32Sim *msim = reinterpret_cast<Mips32Sim *>(data);
        msim->repaintDisplayWindow();
        repaintFlag = false;
    }
    Fl::repeat_timeout(0.02, repaintTimerCallback, data);
}

Mips32Sim::Mips32Sim(VGATextWindow *vgatw):
    m32soc(new VMips32soc), vgatw(vgatw) {
    m32soc->fclk = 0;
    m32soc->rst = 0;
    m32soc->eval();  // Execute initial blocks

    uint32_t *vgaram = m32soc->Mips32soc->vgaTc->frameBuff->memory;
    uint8_t *vgarom = m32soc->Mips32soc->vgaTc->fontRom->memory;
    uint8_t *vgapal = m32soc->Mips32soc->vgaTc->palRom->memory;

    vgatw->initDisplay(vgaram, vgarom, vgapal);
    Fl::add_timeout(0.02, repaintTimerCallback, reinterpret_cast<void *>(this));
}

Mips32Sim::~Mips32Sim() {
    m32soc->final();
}

void Mips32Sim::repaintDisplayWindow() {
    vgatw->redraw();
}

void Mips32Sim::dumpRegFile() {
    uint32_t *rf = m32soc->Mips32soc->regFile->memory;
    for (int i = 0; i < 32; i++) {
        printf("%s = %08x\n", reg_names[i], rf[i]);
    }
}

inline void printChar(char ch) {
    if (!(ch >=32 && ch <= 128)) {
        ch = '?';
    }
    std::cout << ch;
}

inline void printWord(uint32_t w) {
    printChar(static_cast<char>(w >> 24));
    printChar(static_cast<char>(w >> 16));
    printChar(static_cast<char>(w >> 8));
    printChar(static_cast<char>(w >> 0));
}

void Mips32Sim::dumpDataMemoryAscii() {
    int count = 0;
    uint32_t *mem = m32soc->Mips32soc->dataMem->memory;
    for (int i = 0; i < 32; i++) {
        printWord(mem[i]);
        count++;
        if (count == 16) {
            std::cout << '\n';
            count = 0;
        }
    }
}
void Mips32Sim::dumpDataMemory() {
    int count = 0;

    printf("Data memory\n");
    uint32_t *mem = m32soc->Mips32soc->dataMem->memory;
    for (int i = 0; i < 32; i++) {
        printf("%08X ", mem[i]);
        count++;
        if (count == 16) {
            printf("\n");
            count = 0;
        }
    }
}

uint32_t Mips32Sim::getDataAt(uint32_t addr) {
    uint32_t phys_addr = (addr - 0x10010000) / 4;
    uint32_t *mem = m32soc->Mips32soc->dataMem->memory;

    return mem[phys_addr];
}

void Mips32Sim::run() {
    m32soc->keypad = 0xff;
    m32soc->fclk = 0;
    m32soc->rst = 0;
    m32soc->eval();

    m32soc->fclk = 1;
    m32soc->eval();

    m32soc->fclk = 0;
    m32soc->rst = 1;
    m32soc->eval();

    while (!Verilated::gotFinish()) {
        /*dumpRegFile();
        uint32_t pc = m32soc->Mips32soc->pc;
        if (pc >= 0x401264){
            uint32_t mem_index = (pc - 0x400000) / 4;
            uint32_t inst = (mem_index < 2048)? m32soc->Mips32soc->instMem->memory[mem_index] : 0;

            std::cout << "FCLK " << static_cast<int>(m32soc->fclk)
                    << ", RST " << static_cast<int>(m32soc->rst)
                    << ", PC " << std::setw(8) << std::setfill('0') << std::hex << std::showbase << pc 
                    << ", Inst = " << inst << '\n';
            std::string ans;
            std::getline(std::cin, ans);
            if (std::cin.eof()) {
                break;
            }
            if (ans == "n" || ans == "no" || ans == "NO" || ans == "N") {
                Verilated::gotFinish(true);
                break;
            }
        }*/
        /*std::cout<< "pc: "<<  m32soc->Mips32soc->pc<<std::hex <<"addr: "<< m32soc->Mips32soc->addr << " en:"<< (int)m32soc->Mips32soc->toInotO
        << " msc: "<< m32soc->Mips32soc->msc << " dt: "<< m32soc->Mips32soc->InotO << endl;*/
        if (vgatw->stopKeyPressed()) {
            Verilated::gotFinish(true);
        } else {
            m32soc->rst = vgatw->resetKeyPressed()? 0 : 1; // Active LOW
            m32soc->keypad = vgatw->getKeypadState();
            m32soc->fclk = !m32soc->fclk;
            m32soc->eval();

            if (m32soc->iAddr || m32soc->iOpc || m32soc->iPC) {
                Verilated::gotFinish(true);
            }
        }
    }
    
    dumpDataMemoryAscii();
    dumpRegFile();
    uint32_t pc = m32soc->Mips32soc->pc;

    std::cout << "Processor stopped at PC = " << std::setw(8) 
              << std::setfill('0') << std::hex << std::showbase << pc << '\n';

    if (m32soc->iPC) {
        std::cerr << "CPU halted, due to invalid instruction address. PC = " 
                  << std::setw(8) << std::setfill('0') << std::hex << std::showbase << pc << '\n';
    }

    if (m32soc->iAddr) {
        uint32_t addr = m32soc->Mips32soc->aRes;

        std::cerr << "CPU halted, due to invalid data address. PC = " 
                  << std::setw(8) << std::setfill('0') << std::hex << std::showbase << pc
                  << ", Address = " << addr << '\n';
    }

    if (m32soc->iOpc) {
        uint32_t mem_index = (pc - 0x400000) / 4;
        uint32_t inst = m32soc->Mips32soc->instMem->memory[mem_index];

        std::cerr << "CPU halted, due to invalid opcode. PC = " 
                  << std::setw(8) << std::setfill('0') << std::hex << std::showbase << pc
                  << ", Instruction = " << inst << '\n';
    }

    Fl::lock();
    vgatw->hide();
    Fl::unlock();
    Fl::awake();
}

uint32_t millis() {
    
    static auto time1 = std::chrono::system_clock::now();
    //std::cout << "666" <<std::endl;
    auto time2 = std::chrono::system_clock::now();
    auto durms = std::chrono::duration_cast<std::chrono::milliseconds>(time2 - time1);

    return durms.count();
}

void updateVGADisplay() {
    repaintFlag = true;
}
