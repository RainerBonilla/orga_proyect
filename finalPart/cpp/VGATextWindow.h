#ifndef _VGA_DISPLAY_H

#define _VGA_DISPLAY_H

#include <cstdint>
#include <iostream>
#include <vector>
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include "FL/fl_draw.H"
#include "bmp.h"

#define CHAR_WIDTH      8
#define CHAR_HEIGHT     16
#define BYTES_PER_ROW   ((CHAR_WIDTH + 7) / 8)
#define CHAR_SIZE_BYTES ((BYTES_PER_ROW) * (CHAR_HEIGHT))
#define ROW_MSB         7
#define VGA_COLS        80
#define VGA_ROWS        30
#define VGA_NUM_COLORS  16
#define VGA_NUM_SYMBOLS 256

using Point = std::pair<double, double>;
using SymbPixels = std::vector<Point>;

class VGATextWindow: public Fl_Window {
public:
    VGATextWindow(int width, int height):
        Fl_Window(width, height, "VGA Display"),
        vgafb(nullptr),
        kpstate(0xff),
        rstKeyState(false),
        stopKeyState(false) {}

    ~VGATextWindow() {}

    void draw() override;
    int handle(int e) override;
    void loadVGAFont(const uint8_t *vgafont);
    void initColorPalette(const uint8_t *vgapal);
    bool initDisplay(uint32_t *vgafb, uint8_t *vgarom, uint8_t *vgapal);
    void initVGAContent();
    void saveScreenshot(const char *filename);
    bool windowClosed();

    void setVGAFrameBuffer(uint32_t *vgafb) {
        this->vgafb = vgafb;
    }

    uint8_t getKeypadState() { return kpstate; }
    bool resetKeyPressed() { return rstKeyState; }
    bool resetKeyReleased() { return !rstKeyState; }
    bool stopKeyPressed() { return stopKeyState; }
    bool stopKeyReleased() { return !stopKeyState; }
    int getWidth() { return w(); }
    int getHeight() { return h(); }

private:
    void loadVGAFontSymbol(const uint8_t *symbData, SymbPixels& symbPixels);
    void drawVGAContent();
    void drawVGASymbol(int x, int y, uint16_t symb);
    void drawChar(int x, int y, Fl_Color &fgcolor, Fl_Color &bgcolor, uint8_t charIndex);

    Fl_Color getColorFromPalette(uint8_t idx) {
        if (idx < 16) {
            return pal[idx];
        } else {
            std::cerr << "Invalid color index '" << idx << "'\n";
            return FL_BLACK;
        }
    }

private:
    bool rstKeyState;
    bool stopKeyState;
    uint8_t kpstate;
    uint32_t *vgafb;
    std::vector<SymbPixels> vgaSymbs;
    Fl_Color pal[VGA_NUM_COLORS];
};

#endif
