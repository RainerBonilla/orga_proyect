/*
 * MIPS32Sim.h
 *
 *  Created on: Sept 21, 2018
 *      Author: ideras
 */

#ifndef MIPS32SIM_H_
#define MIPS32SIM_H_

#include <cstdint>
#include <memory>

class VMips32soc;
class VGATextWindow;

class Mips32Sim {
public:
    Mips32Sim(VGATextWindow *vgatw);
    ~Mips32Sim();

    void repaintDisplayWindow();
    void dumpRegFile();
    void dumpDataMemory();
    void dumpDataMemoryAscii();
    void run();
private:
    uint32_t getDataAt(uint32_t addr);

private:
    VGATextWindow *vgatw;
    std::unique_ptr<VMips32soc> m32soc;
};

uint32_t millis();
void updateVGADisplay();

#endif /* MIPS32SIM_H_ */
