module DualPortVGARam (
    input clka,
    input enablea,
    input [3:0] wea,
    input [10:0] addra,
    input [31:0] wda,
    output reg [31:0] rda,
    input clkb,
    input[10:0] addrb,
    output reg [31:0] rdb
);

`ifdef verilator
`systemc_imp_header
 #include "Mips32SocSim.h" 
`verilog
`endif

    reg [31:0] memory [0:2047] /*verilator public*/;

    always @ (posedge clka)
    begin
        if (enablea) begin
            if (wea[3]) memory[addra][7:0] <= wda[7:0];
            if (wea[2]) memory[addra][15:8] <= wda[15:8];
            if (wea[1]) memory[addra][23:16] <= wda[23:16];
            if (wea[0]) memory[addra][31:24] <= wda[31:24];
            
            rda <= memory[addra];

`ifdef verilator
            $c("updateVGADisplay();");
`endif
        end
    end

    always @ (posedge clkb) begin
        rdb <= memory[addrb];
    end
endmodule

/* verilator lint_off UNDRIVEN */
module FontRom (
    input clk,
    input [11:0] addr,
    output reg [7:0] dout
    );

    reg[7:0] memory[0:4095] /*verilator public*/;

    always @(posedge clk) begin
        dout <= memory[addr];
    end

    initial begin
        $readmemh("font_rom.mif", memory, 0, 4095);
    end
endmodule

module RomColorPalette (
    input  [3:0] addr1,
    input  [3:0] addr2,
    output [7:0] color1,
    output [7:0] color2
);

  reg[7:0] memory[0:15] /*verilator public*/;

  assign color1 = memory[addr1];
  assign color2 = memory[addr2];

  initial begin
        memory[0] = 8'b00000000; // Black
        memory[1] = 8'b00000010; // Blue
        memory[2] = 8'b00010100; // Green
        memory[3] = 8'b00010110; // Cyan
        memory[4] = 8'b10100000; // Red
        memory[5] = 8'b10100010; // Magenta
        memory[6] = 8'b10101000; // Brown
        memory[7] = 8'b10110110; // White
        memory[8] = 8'b01001001; // Grey
        memory[9] = 8'b01001011; // Light blue
        memory[10] = 8'b01011101; // Light green
        memory[11] = 8'b01011111; // Light cyan
        memory[12] = 8'b11101001; // Light red
        memory[13] = 8'b11101011; // Light magenta
        memory[14] = 8'b11111101; // Yellow
        memory[15] = 8'b11111111; // Bright white
  end
endmodule // RomColorPalette

/* verilator lint_off WIDTH */
module VGA80X30Color(
    input clk,  // 25 Mhz for 640x480 resolution
    input reset,
    output [10:0] vgaram_addr,  // vga memory address
    input [31:0] vgaram_data,   // vga memory data
    output [11:0] fontrom_addr, // Font ROM address
    input [7:0] fontrom_data,   // Font ROM data
    output [3:0] palrom_addr1,  // Palette ROM address 1
    output [3:0] palrom_addr2,  // Palette ROM address 2
    output [7:0] palrom_data1,  // Palette ROM data 1
    output [7:0] palrom_data2,  // Palette ROM data 2
    output reg [2:0] red,
    output reg [2:0] green,
    output reg [1:0] blue,
    output reg hsync,
    output reg vsync
);

    reg [9:0] hcount;
    reg [9:0] vcount;

    wire [4:0] scr_row; // Screen row (0 to 29)
    wire [6:0] scr_col; // Screen column (0 to 79)
    wire [3:0] chr_row; // Character row (0 to 15)
    wire [2:0] chr_col; // Character column (0 to 7)
    wire [7:0] chr_index;
    wire [11:0] symb_index; // The index of the current symbol to display
    wire [15:0] symb_data; // The data of the current symbol to display
    reg  [7:0] fg_color; // Foreground color
    reg  [7:0] bg_color; // Background color
    reg  [7:0] chr_data;

    assign symb_data = (symb_index[0] == 1'b0)? vgaram_data[31:16] : vgaram_data[15:0]; 
    assign chr_index = symb_data[7:0];
    assign scr_row = vcount[8:4]; // divide by 16
    assign scr_col = hcount[9:3]; // divide by 8

    assign palrom_addr1 = symb_data[15:12];
    assign palrom_addr2 = symb_data[11:8];
    assign chr_row = vcount[3:0]; // mod 16
    assign chr_col = ~hcount[2:0]; // mod 8 (Inverted 'cause columns go from 7 to 0 in the ROM)
    assign symb_index = scr_row * 80 + {7'h00, scr_col};
    assign fontrom_addr = chr_index * 16 + {8'h00, chr_row};
    assign vgaram_addr = symb_index[11:1]; // Divide by 2

    always @ (posedge clk)
    begin
      if (chr_col == 3'h0) begin
          chr_data <= fontrom_data;
          bg_color <= palrom_data1;
          fg_color <= palrom_data2;
      end
    end

    always @ (posedge clk)
    begin
        if (~reset)
        begin
            hcount <= 10'h0;
            vcount <= 10'h0;
        end
        else begin
            if (hcount == 799) begin
                hcount <= 0;

                if (vcount == 524)
                    vcount <= 0;
                else
                    vcount <= vcount + 1;
            end
            else
                hcount <= hcount + 1;

            if ((vcount >= 490) && (vcount < 492))
                vsync <= 0; //Vertical sync pulse
            else
                vsync <= 1;

            if ((hcount >= 656) && (hcount < 752))
                hsync <= 0; //Horizontal sync pulse
            else
                hsync <= 1;

            if (((hcount > 7) && (hcount < 648)) && (vcount < 480))
            begin
                if (chr_data[chr_col] == 1'b1)
                    {red, green, blue} <= fg_color;
                else
                    {red, green, blue} <= bg_color;
            end
            else begin
                {red, green, blue} <= 8'h0;
            end
        end
    end
endmodule

/* verilator lint_off UNUSED */
/* verilator lint_off UNDRIVEN */
/*!
 * VGA Text Graphics Card
 * @type ram
 * @size 4096
 * @data_bits 8
 * @class MyVGATextCard
 * @create vgaTextCard_create
 * @eval vgaTextCard_eval
 * @destroy vgaTextCard_destroy
 */
module VGATextCard (
    input vclk, //! VGA clock input 25Mhz for 640x480 resolution. Not used in Digital
    input clk, //! Clock
    input rst, //! Reset
    input en, //! Enable 
    input [3:0] we, //! Write enable
    input  [10:0] addr, //! Address
    input  [31:0] wd, //! Write data
    output [31:0] rd, //! Read data
    output [2:0] r, //! Red output. Not used in Digital
    output [2:0] g, //! Green output. Not used in Digital
    output [1:0] b, //! Blue output. Not used in Digital
    output hs,  //! Horizontal Sync. Not used in Digital
    output vs //! Vertical Sync. Not used in Digital
);

    // Text Buffer RAM Memory Signals, Port B (to VGA core)
    wire [31:0] rd2;
    wire [10:0] addr2;

    // Font ROM Memory Signals
    wire [11:0] fontrom_addr;
    wire [7:0]  fontrom_rd;

    // Palette ROM signal
    wire [3:0] palrom_addr1;
    wire [3:0] palrom_addr2;
    wire [7:0] palrom_data1;
    wire [7:0] palrom_data2;

`ifdef SYNTHESIS
    VGA80X30Color vgaproto (
        .clk(vclk),
        .rst(rst),
        .vgaram_addr(addr2), // vga memory address
        .vgaram_data(rd2),   // vga memory data
        .fontrom_addr(fontrom_addr),  // Font ROM memory address
        .fontrom_data(fontrom_rd),    // Font ROM memory data
        .palrom_addr1(palrom_addr1),
        .palrom_addr2(palrom_addr2),
        .palrom_data1(palrom_data1),
        .palrom_data2(palrom_data2),
        .r(r),
        .g(g),
        .b(b),
        .hs(hs),
        .vs(vs)
    );
`else
    /*
     * Generate some DUMMY code so
     * verilator doesn't remove the
     * Font Rom and the Palette ROM
     */
    assign r = palrom_data1[2:0];
    assign g = fontrom_rd[2:0];
        
`endif    

    RomColorPalette palRom (
      .addr1(palrom_addr1),
      .addr2(palrom_addr2),
      .color1(palrom_data1),
      .color2(palrom_data2)
    );

    DualPortVGARam frameBuff (
        .clka(clk),
        .enablea(en),
        .wea(we),
        .addra(addr),
        .rda(rd),
        .wda(wd),
        .clkb(vclk),
        .addrb(addr2),
        .rdb(rd2)
    );

    FontRom fontRom (
      .clk(vclk),
      .addr(fontrom_addr),
      .dout(fontrom_rd)
    );
endmodule
