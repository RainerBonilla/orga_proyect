/* verilator lint_off UNUSED */
/* verilator lint_off UNDRIVEN */
/*!
 * Milliseconds counter component.
 * @class MyCounter
 * @create msCounter_create
 * @eval msCounter_eval
 */
module MsCounter(
    input clk, //! Assumes a 50MHz
    input rst,
    output [31:0] out
);

`ifdef verilator
`systemc_imp_header
 #include "Mips32SocSim.h" 
`verilog
`endif

    reg [15:0] cycle_count; // CPU cycle counter
    reg [31:0] ms_count;    // Millisecond counter

    assign out = ms_count;

    always @ (posedge clk) begin
        if (~rst) begin
            cycle_count <= 16'h0;
            ms_count <= 32'h0;
        end
        else begin
`ifdef verilator
            ms_count <= $c("millis()");
`else
            cycle_count <= cycle_count + 1'b1;
            if (cycle_count == 16'd50000)
            begin
                ms_count <= ms_count + 1;
                cycle_count <= 'h0;
            end
`endif
        end
    end
endmodule
