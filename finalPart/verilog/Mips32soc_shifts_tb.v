//  A testbench for Mips32soc_shifts_tb
`timescale 1us/1ns

module Mips32soc_shifts_tb;
    reg fclk;
    reg rst;
    reg [7:0] keypad;
    wire [2:0] green;
    wire [1:0] blue;
    wire hsync;
    wire vsync;
    wire [2:0] red;
    wire iAddr;
    wire iOpc;
    wire iPC;
    wire err;

  Mips32soc Mips32soc0 (
    .fclk(fclk),
    .rst(rst),
    .keypad(keypad),
    .green(green),
    .blue(blue),
    .hsync(hsync),
    .vsync(vsync),
    .red(red),
    .iAddr(iAddr),
    .iOpc(iOpc),
    .iPC(iPC),
    .err(err)
  );

