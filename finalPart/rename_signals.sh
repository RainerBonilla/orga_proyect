#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Missing file argument"
    exit 1
fi

FILE=$1

cat $FILE| fgrep "wire [31:0] pc;" &> /dev/null
if [ $? -ne 0 ]; then
    echo "Couldn't find the 'pc' wire. Did you forget to add the tunnel in Digital to the Program Counter register?"
    exit 1
fi

cat $FILE| fgrep "wire [31:0] aRes;" &> /dev/null
if [ $? -ne 0 ]; then
    echo "Couldn't find the 'aRes' wire. Did you forget to add the tunnel in Digital to the ALU result?"
    exit 1
fi

sed -i -e 's/RegFile[ ]\+[a-zA-Z0-9_]\+/RegFile regFile/' -e 's/InstMem[ ]\+[a-zA-Z0-9_]\+/InstMem instMem/'  -e 's/DataMem[ ]\+[a-zA-Z0-9_]\+/DataMem dataMem/' -e 's/VGATextCard[ ]\+[a-zA-Z0-9_]\+/VGATextCard vgaTc/' -e 's/wire \[31:0\] pc;/wire [31:0] pc \/*verilator public*\/;/' -e 's/wire \[31:0\] aRes;/wire [31:0] aRes \/*verilator public*\/;/' $FILE