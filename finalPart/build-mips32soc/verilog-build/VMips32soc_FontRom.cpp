// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VMips32soc.h for the primary calling header

#include "VMips32soc_FontRom.h" // For This
#include "VMips32soc__Syms.h"

#include "verilated_dpi.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VMips32soc_FontRom) {
    // Reset internal values
    // Reset structure values
    _ctor_var_reset();
}

void VMips32soc_FontRom::__Vconfigure(VMips32soc__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VMips32soc_FontRom::~VMips32soc_FontRom() {
}

//--------------------
// Internal Methods

VL_INLINE_OPT void VMips32soc_FontRom::_sequent__TOP__Mips32soc__vgaTc__fontRom__1(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          VMips32soc_FontRom::_sequent__TOP__Mips32soc__vgaTc__fontRom__1\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:232
    vlSymsp->TOP__Mips32soc__vgaTc__fontRom.__PVT__dout 
	= vlSymsp->TOP__Mips32soc__vgaTc__fontRom.memory
	[vlSymsp->TOP__Mips32soc__vgaTc.__PVT__fontrom_addr];
}

void VMips32soc_FontRom::_initial__TOP__Mips32soc__vgaTc__fontRom__2(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          VMips32soc_FontRom::_initial__TOP__Mips32soc__vgaTc__fontRom__2\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    VL_SIGW(__Vtemp1,95,0,3);
    // Body
    // INITIAL at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:236
    __Vtemp1[0U] = 0x2e6d6966U;
    __Vtemp1[1U] = 0x5f726f6dU;
    __Vtemp1[2U] = 0x666f6e74U;
    VL_READMEM_W (true,8,4096, 0,3, __Vtemp1, vlSymsp->TOP__Mips32soc__vgaTc__fontRom.memory
		  ,0U,0xfffU);
}

void VMips32soc_FontRom::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("          VMips32soc_FontRom::_ctor_var_reset\n"); );
    // Body
    __PVT__clk = VL_RAND_RESET_I(1);
    __PVT__addr = VL_RAND_RESET_I(12);
    __PVT__dout = VL_RAND_RESET_I(8);
    { int __Vi0=0; for (; __Vi0<4096; ++__Vi0) {
	    memory[__Vi0] = VL_RAND_RESET_I(8);
    }}
}

void VMips32soc_FontRom::_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("          VMips32soc_FontRom::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
