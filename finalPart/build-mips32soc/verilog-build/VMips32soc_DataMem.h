// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See VMips32soc.h for the primary calling header

#ifndef _VMips32soc_DataMem_H_
#define _VMips32soc_DataMem_H_

#include "verilated.h"
#include "VMips32soc__Dpi.h"

class VMips32soc__Syms;

//----------

VL_MODULE(VMips32soc_DataMem) {
  public:
    // CELLS
    
    // PORTS
    VL_IN8(__PVT__clk,0,0);
    VL_IN8(__PVT__en,0,0);
    VL_IN8(__PVT__we,3,0);
    //char	__VpadToAlign3[1];
    VL_IN16(__PVT__addr,10,0);
    //char	__VpadToAlign6[2];
    VL_IN(__PVT__wd,31,0);
    VL_OUT(__PVT__rd,31,0);
    
    // LOCAL SIGNALS
    //char	__VpadToAlign20[4];
    VL_SIG(memory[2048],31,0);
    
    // LOCAL VARIABLES
    VL_SIG8(__Vdlyvlsb__memory__v0,4,0);
    VL_SIG8(__Vdlyvval__memory__v0,7,0);
    VL_SIG8(__Vdlyvset__memory__v0,0,0);
    VL_SIG8(__Vdlyvlsb__memory__v1,4,0);
    VL_SIG8(__Vdlyvval__memory__v1,7,0);
    VL_SIG8(__Vdlyvset__memory__v1,0,0);
    VL_SIG8(__Vdlyvlsb__memory__v2,4,0);
    VL_SIG8(__Vdlyvval__memory__v2,7,0);
    VL_SIG8(__Vdlyvset__memory__v2,0,0);
    VL_SIG8(__Vdlyvlsb__memory__v3,4,0);
    VL_SIG8(__Vdlyvval__memory__v3,7,0);
    VL_SIG8(__Vdlyvset__memory__v3,0,0);
    VL_SIG16(__Vdlyvdim0__memory__v0,10,0);
    VL_SIG16(__Vdlyvdim0__memory__v1,10,0);
    VL_SIG16(__Vdlyvdim0__memory__v2,10,0);
    VL_SIG16(__Vdlyvdim0__memory__v3,10,0);
    
    // INTERNAL VARIABLES
  private:
    //char	__VpadToAlign8244[4];
    VMips32soc__Syms*	__VlSymsp;		// Symbol table
  public:
    
    // PARAMETERS
    
    // CONSTRUCTORS
  private:
    VMips32soc_DataMem& operator= (const VMips32soc_DataMem&);	///< Copying not allowed
    VMips32soc_DataMem(const VMips32soc_DataMem&);	///< Copying not allowed
  public:
    VMips32soc_DataMem(const char* name="TOP");
    ~VMips32soc_DataMem();
    
    // USER METHODS
    
    // API METHODS
    
    // INTERNAL METHODS
    void __Vconfigure(VMips32soc__Syms* symsp, bool first);
  private:
    void	_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first);
    void	_ctor_var_reset();
  public:
    static void	_initial__TOP__Mips32soc__dataMem__2(VMips32soc__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__Mips32soc__dataMem__1(VMips32soc__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
