// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VMips32soc.h for the primary calling header

#include "VMips32soc_DataMem.h" // For This
#include "VMips32soc__Syms.h"

#include "verilated_dpi.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VMips32soc_DataMem) {
    // Reset internal values
    // Reset structure values
    _ctor_var_reset();
}

void VMips32soc_DataMem::__Vconfigure(VMips32soc__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VMips32soc_DataMem::~VMips32soc_DataMem() {
}

//--------------------
// Internal Methods

VL_INLINE_OPT void VMips32soc_DataMem::_sequent__TOP__Mips32soc__dataMem__1(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("        VMips32soc_DataMem::_sequent__TOP__Mips32soc__dataMem__1\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvset__memory__v0 = 0U;
    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvset__memory__v1 = 0U;
    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvset__memory__v2 = 0U;
    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvset__memory__v3 = 0U;
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:485
    if ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__me))) {
	if ((8U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__outwe))) {
	    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvval__memory__v0 
		= (0xffU & vlSymsp->TOP__Mips32soc.__PVT__outdata);
	    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvset__memory__v0 = 1U;
	    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvlsb__memory__v0 = 0U;
	    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvdim0__memory__v0 
		= (0x7ffU & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__s16) 
			     >> 2U));
	}
	if ((4U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__outwe))) {
	    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvval__memory__v1 
		= (0xffU & (vlSymsp->TOP__Mips32soc.__PVT__outdata 
			    >> 8U));
	    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvset__memory__v1 = 1U;
	    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvlsb__memory__v1 = 8U;
	    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvdim0__memory__v1 
		= (0x7ffU & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__s16) 
			     >> 2U));
	}
	if ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__outwe))) {
	    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvval__memory__v2 
		= (0xffU & (vlSymsp->TOP__Mips32soc.__PVT__outdata 
			    >> 0x10U));
	    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvset__memory__v2 = 1U;
	    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvlsb__memory__v2 = 0x10U;
	    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvdim0__memory__v2 
		= (0x7ffU & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__s16) 
			     >> 2U));
	}
	if ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__outwe))) {
	    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvval__memory__v3 
		= (0xffU & (vlSymsp->TOP__Mips32soc.__PVT__outdata 
			    >> 0x18U));
	    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvset__memory__v3 = 1U;
	    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvlsb__memory__v3 = 0x18U;
	    vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvdim0__memory__v3 
		= (0x7ffU & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__s16) 
			     >> 2U));
	}
	vlSymsp->TOP__Mips32soc__dataMem.__PVT__rd 
	    = vlSymsp->TOP__Mips32soc__dataMem.memory
	    [(0x7ffU & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__s16) 
			>> 2U))];
    }
    // ALWAYSPOST at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:488
    if (vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvset__memory__v0) {
	vlSymsp->TOP__Mips32soc__dataMem.memory[vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvdim0__memory__v0] 
	    = (((~ ((IData)(0xffU) << (IData)(vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvlsb__memory__v0))) 
		& vlSymsp->TOP__Mips32soc__dataMem.memory
		[vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvdim0__memory__v0]) 
	       | ((IData)(vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvval__memory__v0) 
		  << (IData)(vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvlsb__memory__v0)));
    }
    if (vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvset__memory__v1) {
	vlSymsp->TOP__Mips32soc__dataMem.memory[vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvdim0__memory__v1] 
	    = (((~ ((IData)(0xffU) << (IData)(vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvlsb__memory__v1))) 
		& vlSymsp->TOP__Mips32soc__dataMem.memory
		[vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvdim0__memory__v1]) 
	       | ((IData)(vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvval__memory__v1) 
		  << (IData)(vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvlsb__memory__v1)));
    }
    if (vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvset__memory__v2) {
	vlSymsp->TOP__Mips32soc__dataMem.memory[vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvdim0__memory__v2] 
	    = (((~ ((IData)(0xffU) << (IData)(vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvlsb__memory__v2))) 
		& vlSymsp->TOP__Mips32soc__dataMem.memory
		[vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvdim0__memory__v2]) 
	       | ((IData)(vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvval__memory__v2) 
		  << (IData)(vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvlsb__memory__v2)));
    }
    if (vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvset__memory__v3) {
	vlSymsp->TOP__Mips32soc__dataMem.memory[vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvdim0__memory__v3] 
	    = (((~ ((IData)(0xffU) << (IData)(vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvlsb__memory__v3))) 
		& vlSymsp->TOP__Mips32soc__dataMem.memory
		[vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvdim0__memory__v3]) 
	       | ((IData)(vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvval__memory__v3) 
		  << (IData)(vlSymsp->TOP__Mips32soc__dataMem.__Vdlyvlsb__memory__v3)));
    }
}

void VMips32soc_DataMem::_initial__TOP__Mips32soc__dataMem__2(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("        VMips32soc_DataMem::_initial__TOP__Mips32soc__dataMem__2\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // INITIAL at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:497
    VL_READMEM_Q (true,32,2048, 0,2, VL_ULL(0x646174612e6d6966)
		  , vlSymsp->TOP__Mips32soc__dataMem.memory
		  ,0U,0x3ffU);
}

void VMips32soc_DataMem::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("        VMips32soc_DataMem::_ctor_var_reset\n"); );
    // Body
    __PVT__clk = VL_RAND_RESET_I(1);
    __PVT__en = VL_RAND_RESET_I(1);
    __PVT__we = VL_RAND_RESET_I(4);
    __PVT__addr = VL_RAND_RESET_I(11);
    __PVT__wd = VL_RAND_RESET_I(32);
    __PVT__rd = VL_RAND_RESET_I(32);
    { int __Vi0=0; for (; __Vi0<2048; ++__Vi0) {
	    memory[__Vi0] = VL_RAND_RESET_I(32);
    }}
    __Vdlyvdim0__memory__v0 = VL_RAND_RESET_I(11);
    __Vdlyvlsb__memory__v0 = VL_RAND_RESET_I(5);
    __Vdlyvval__memory__v0 = VL_RAND_RESET_I(8);
    __Vdlyvset__memory__v0 = VL_RAND_RESET_I(1);
    __Vdlyvdim0__memory__v1 = VL_RAND_RESET_I(11);
    __Vdlyvlsb__memory__v1 = VL_RAND_RESET_I(5);
    __Vdlyvval__memory__v1 = VL_RAND_RESET_I(8);
    __Vdlyvset__memory__v1 = VL_RAND_RESET_I(1);
    __Vdlyvdim0__memory__v2 = VL_RAND_RESET_I(11);
    __Vdlyvlsb__memory__v2 = VL_RAND_RESET_I(5);
    __Vdlyvval__memory__v2 = VL_RAND_RESET_I(8);
    __Vdlyvset__memory__v2 = VL_RAND_RESET_I(1);
    __Vdlyvdim0__memory__v3 = VL_RAND_RESET_I(11);
    __Vdlyvlsb__memory__v3 = VL_RAND_RESET_I(5);
    __Vdlyvval__memory__v3 = VL_RAND_RESET_I(8);
    __Vdlyvset__memory__v3 = VL_RAND_RESET_I(1);
}

void VMips32soc_DataMem::_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("        VMips32soc_DataMem::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
