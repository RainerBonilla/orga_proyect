// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VMips32soc.h for the primary calling header

#include "VMips32soc.h"        // For This
#include "VMips32soc__Syms.h"

#include "verilated_dpi.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VMips32soc) {
    VMips32soc__Syms* __restrict vlSymsp = __VlSymsp = new VMips32soc__Syms(this, name());
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    VL_CELL (Mips32soc, VMips32soc_Mips32soc);
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void VMips32soc::__Vconfigure(VMips32soc__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VMips32soc::~VMips32soc() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void VMips32soc::eval() {
    VMips32soc__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate VMips32soc::eval\n"); );
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void VMips32soc::_eval_initial_loop(VMips32soc__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

void VMips32soc::_settle__TOP__1(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VMips32soc::_settle__TOP__1\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->blue = vlSymsp->TOP__Mips32soc__vgaTc.__PVT__b;
    vlTOPp->hsync = vlSymsp->TOP__Mips32soc__vgaTc.__PVT__hs;
    vlTOPp->vsync = vlSymsp->TOP__Mips32soc__vgaTc.__PVT__vs;
}

VL_INLINE_OPT void VMips32soc::_sequent__TOP__2(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VMips32soc::_sequent__TOP__2\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->green = (7U & (IData)(vlSymsp->TOP__Mips32soc__vgaTc__fontRom.__PVT__dout));
}

void VMips32soc::_settle__TOP__3(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VMips32soc::_settle__TOP__3\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->green = (7U & (IData)(vlSymsp->TOP__Mips32soc__vgaTc__fontRom.__PVT__dout));
    vlTOPp->red = (7U & (IData)(vlSymsp->TOP__Mips32soc__vgaTc__palRom.__PVT__color1));
    vlTOPp->red = (7U & (IData)(vlSymsp->TOP__Mips32soc__vgaTc__palRom.__PVT__color1));
}

VL_INLINE_OPT void VMips32soc::_settle__TOP__4(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VMips32soc::_settle__TOP__4\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->iOpc = vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp;
}

VL_INLINE_OPT void VMips32soc::_sequent__TOP__6(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VMips32soc::_sequent__TOP__6\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->iAddr = vlSymsp->TOP__Mips32soc.__PVT__iAddr_temp;
}

VL_INLINE_OPT void VMips32soc::_combo__TOP__8(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VMips32soc::_combo__TOP__8\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->iPC = vlSymsp->TOP__Mips32soc.__PVT__iPC_temp;
}

VL_INLINE_OPT void VMips32soc::_settle__TOP__10(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VMips32soc::_settle__TOP__10\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->err = vlSymsp->TOP__Mips32soc.__PVT__err_temp;
}

void VMips32soc::_eval(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VMips32soc::_eval\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (((IData)(vlTOPp->fclk) & (~ (IData)(vlTOPp->__Vclklast__TOP__fclk)))) {
	vlSymsp->TOP__Mips32soc__vgaTc__fontRom._sequent__TOP__Mips32soc__vgaTc__fontRom__1(vlSymsp);
	vlSymsp->TOP__Mips32soc__dataMem._sequent__TOP__Mips32soc__dataMem__1(vlSymsp);
	vlSymsp->TOP__Mips32soc._sequent__TOP__Mips32soc__2(vlSymsp);
	vlTOPp->_sequent__TOP__2(vlSymsp);
    }
    if (((IData)(vlTOPp->__VinpClk__TOP__Mips32soc____PVT__cclk) 
	 & (~ (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__Mips32soc____PVT__cclk)))) {
	vlSymsp->TOP__Mips32soc._sequent__TOP__Mips32soc__4(vlSymsp);
	vlSymsp->TOP__Mips32soc__vgaTc__frameBuff._sequent__TOP__Mips32soc__vgaTc__frameBuff__2(vlSymsp);
	vlSymsp->TOP__Mips32soc__regFile._sequent__TOP__Mips32soc__regFile__2(vlSymsp);
	vlSymsp->TOP__Mips32soc._sequent__TOP__Mips32soc__5(vlSymsp);
	vlSymsp->TOP__Mips32soc__instMem._sequent__TOP__Mips32soc__instMem__2(vlSymsp);
	vlSymsp->TOP__Mips32soc._sequent__TOP__Mips32soc__6(vlSymsp);
	vlSymsp->TOP__Mips32soc__regFile._sequent__TOP__Mips32soc__regFile__3(vlSymsp);
	vlTOPp->_settle__TOP__4(vlSymsp);
	vlSymsp->TOP__Mips32soc._sequent__TOP__Mips32soc__10(vlSymsp);
	vlSymsp->TOP__Mips32soc._sequent__TOP__Mips32soc__12(vlSymsp);
	vlTOPp->_sequent__TOP__6(vlSymsp);
    }
    vlSymsp->TOP__Mips32soc._combo__TOP__Mips32soc__13(vlSymsp);
    vlSymsp->TOP__Mips32soc._combo__TOP__Mips32soc__15(vlSymsp);
    vlTOPp->_combo__TOP__8(vlSymsp);
    vlTOPp->_settle__TOP__10(vlSymsp);
    // Final
    vlTOPp->__Vclklast__TOP__fclk = vlTOPp->fclk;
    vlTOPp->__Vclklast__TOP____VinpClk__TOP__Mips32soc____PVT__cclk 
	= vlTOPp->__VinpClk__TOP__Mips32soc____PVT__cclk;
    vlTOPp->__VinpClk__TOP__Mips32soc____PVT__cclk 
	= vlSymsp->TOP__Mips32soc.__PVT__cclk;
}

void VMips32soc::_eval_initial(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VMips32soc::_eval_initial\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlSymsp->TOP__Mips32soc__vgaTc__palRom._initial__TOP__Mips32soc__vgaTc__palRom__1(vlSymsp);
    vlSymsp->TOP__Mips32soc__regFile._initial__TOP__Mips32soc__regFile__1(vlSymsp);
    vlSymsp->TOP__Mips32soc__vgaTc__fontRom._initial__TOP__Mips32soc__vgaTc__fontRom__2(vlSymsp);
    vlSymsp->TOP__Mips32soc__dataMem._initial__TOP__Mips32soc__dataMem__2(vlSymsp);
    vlSymsp->TOP__Mips32soc._initial__TOP__Mips32soc__3(vlSymsp);
    vlSymsp->TOP__Mips32soc._initial__TOP__Mips32soc__9(vlSymsp);
    vlSymsp->TOP__Mips32soc__instMem._initial__TOP__Mips32soc__instMem__3(vlSymsp);
}

void VMips32soc::final() {
    VL_DEBUG_IF(VL_PRINTF("    VMips32soc::final\n"); );
    // Variables
    VMips32soc__Syms* __restrict vlSymsp = this->__VlSymsp;
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VMips32soc::_eval_settle(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VMips32soc::_eval_settle\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_settle__TOP__1(vlSymsp);
    vlSymsp->TOP__Mips32soc._settle__TOP__Mips32soc__1(vlSymsp);
    vlSymsp->TOP__Mips32soc__vgaTc__palRom._settle__TOP__Mips32soc__vgaTc__palRom__2(vlSymsp);
    vlTOPp->_settle__TOP__3(vlSymsp);
    vlSymsp->TOP__Mips32soc._settle__TOP__Mips32soc__7(vlSymsp);
    vlSymsp->TOP__Mips32soc__regFile._sequent__TOP__Mips32soc__regFile__3(vlSymsp);
    vlTOPp->_settle__TOP__4(vlSymsp);
    vlSymsp->TOP__Mips32soc._settle__TOP__Mips32soc__8(vlSymsp);
    vlSymsp->TOP__Mips32soc._settle__TOP__Mips32soc__11(vlSymsp);
    vlSymsp->TOP__Mips32soc._settle__TOP__Mips32soc__14(vlSymsp);
    vlTOPp->_sequent__TOP__6(vlSymsp);
    vlTOPp->_combo__TOP__8(vlSymsp);
    vlSymsp->TOP__Mips32soc._settle__TOP__Mips32soc__16(vlSymsp);
    vlTOPp->_settle__TOP__10(vlSymsp);
}

VL_INLINE_OPT QData VMips32soc::_change_request(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VMips32soc::_change_request\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    __req |= ((vlSymsp->TOP__Mips32soc.__PVT__cclk ^ vlTOPp->__Vchglast__TOP__Mips32soc__cclk));
    VL_DEBUG_IF( if(__req && ((vlSymsp->TOP__Mips32soc.__PVT__cclk ^ vlTOPp->__Vchglast__TOP__Mips32soc__cclk))) VL_PRINTF("	CHANGE: /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:1448: cclk\n"); );
    // Final
    vlTOPp->__Vchglast__TOP__Mips32soc__cclk = vlSymsp->TOP__Mips32soc.__PVT__cclk;
    return __req;
}

void VMips32soc::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("    VMips32soc::_ctor_var_reset\n"); );
    // Body
    fclk = VL_RAND_RESET_I(1);
    rst = VL_RAND_RESET_I(1);
    keypad = VL_RAND_RESET_I(8);
    green = VL_RAND_RESET_I(3);
    blue = VL_RAND_RESET_I(2);
    hsync = VL_RAND_RESET_I(1);
    vsync = VL_RAND_RESET_I(1);
    red = VL_RAND_RESET_I(3);
    iAddr = VL_RAND_RESET_I(1);
    iOpc = VL_RAND_RESET_I(1);
    iPC = VL_RAND_RESET_I(1);
    err = VL_RAND_RESET_I(1);
    __VinpClk__TOP__Mips32soc____PVT__cclk = VL_RAND_RESET_I(1);
    __Vclklast__TOP__fclk = VL_RAND_RESET_I(1);
    __Vclklast__TOP____VinpClk__TOP__Mips32soc____PVT__cclk = VL_RAND_RESET_I(1);
    __Vchglast__TOP__Mips32soc__cclk = VL_RAND_RESET_I(1);
}

void VMips32soc::_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("    VMips32soc::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
