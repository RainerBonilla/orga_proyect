// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See VMips32soc.h for the primary calling header

#ifndef _VMips32soc_DualPortVGARam_H_
#define _VMips32soc_DualPortVGARam_H_

#include "verilated.h"
#include "VMips32soc__Dpi.h"

class VMips32soc__Syms;

//----------

VL_MODULE(VMips32soc_DualPortVGARam) {
  public:
    // CELLS
    
    // PORTS
    VL_IN8(__PVT__clka,0,0);
    VL_IN8(__PVT__clkb,0,0);
    VL_IN8(__PVT__enablea,0,0);
    VL_IN8(__PVT__wea,3,0);
    VL_IN16(__PVT__addra,10,0);
    VL_IN16(__PVT__addrb,10,0);
    VL_IN(__PVT__wda,31,0);
    VL_OUT(__PVT__rda,31,0);
    VL_OUT(__PVT__rdb,31,0);
    
    // LOCAL SIGNALS
    VL_SIG(memory[2048],31,0);
    
    // LOCAL VARIABLES
    VL_SIG8(__Vdlyvlsb__memory__v0,4,0);
    VL_SIG8(__Vdlyvval__memory__v0,7,0);
    VL_SIG8(__Vdlyvset__memory__v0,0,0);
    VL_SIG8(__Vdlyvlsb__memory__v1,4,0);
    VL_SIG8(__Vdlyvval__memory__v1,7,0);
    VL_SIG8(__Vdlyvset__memory__v1,0,0);
    VL_SIG8(__Vdlyvlsb__memory__v2,4,0);
    VL_SIG8(__Vdlyvval__memory__v2,7,0);
    VL_SIG8(__Vdlyvset__memory__v2,0,0);
    VL_SIG8(__Vdlyvlsb__memory__v3,4,0);
    VL_SIG8(__Vdlyvval__memory__v3,7,0);
    VL_SIG8(__Vdlyvset__memory__v3,0,0);
    VL_SIG16(__Vdlyvdim0__memory__v0,10,0);
    VL_SIG16(__Vdlyvdim0__memory__v1,10,0);
    VL_SIG16(__Vdlyvdim0__memory__v2,10,0);
    VL_SIG16(__Vdlyvdim0__memory__v3,10,0);
    
    // INTERNAL VARIABLES
  private:
    //char	__VpadToAlign8244[4];
    VMips32soc__Syms*	__VlSymsp;		// Symbol table
  public:
    
    // PARAMETERS
    
    // CONSTRUCTORS
  private:
    VMips32soc_DualPortVGARam& operator= (const VMips32soc_DualPortVGARam&);	///< Copying not allowed
    VMips32soc_DualPortVGARam(const VMips32soc_DualPortVGARam&);	///< Copying not allowed
  public:
    VMips32soc_DualPortVGARam(const char* name="TOP");
    ~VMips32soc_DualPortVGARam();
    
    // USER METHODS
    
    // API METHODS
    
    // INTERNAL METHODS
    void __Vconfigure(VMips32soc__Syms* symsp, bool first);
  private:
    void	_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first);
    void	_ctor_var_reset();
  public:
    void	_sequent__TOP__Mips32soc__vgaTc__frameBuff__2(VMips32soc__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
