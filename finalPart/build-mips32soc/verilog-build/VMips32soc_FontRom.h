// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See VMips32soc.h for the primary calling header

#ifndef _VMips32soc_FontRom_H_
#define _VMips32soc_FontRom_H_

#include "verilated.h"
#include "VMips32soc__Dpi.h"

class VMips32soc__Syms;

//----------

VL_MODULE(VMips32soc_FontRom) {
  public:
    // CELLS
    
    // PORTS
    VL_IN8(__PVT__clk,0,0);
    VL_OUT8(__PVT__dout,7,0);
    VL_IN16(__PVT__addr,11,0);
    
    // LOCAL SIGNALS
    VL_SIG8(memory[4096],7,0);
    
    // LOCAL VARIABLES
    
    // INTERNAL VARIABLES
  private:
    VMips32soc__Syms*	__VlSymsp;		// Symbol table
  public:
    
    // PARAMETERS
    
    // CONSTRUCTORS
  private:
    VMips32soc_FontRom& operator= (const VMips32soc_FontRom&);	///< Copying not allowed
    VMips32soc_FontRom(const VMips32soc_FontRom&);	///< Copying not allowed
  public:
    VMips32soc_FontRom(const char* name="TOP");
    ~VMips32soc_FontRom();
    
    // USER METHODS
    
    // API METHODS
    
    // INTERNAL METHODS
    void __Vconfigure(VMips32soc__Syms* symsp, bool first);
  private:
    void	_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first);
    void	_ctor_var_reset();
  public:
    static void	_initial__TOP__Mips32soc__vgaTc__fontRom__2(VMips32soc__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__Mips32soc__vgaTc__fontRom__1(VMips32soc__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
