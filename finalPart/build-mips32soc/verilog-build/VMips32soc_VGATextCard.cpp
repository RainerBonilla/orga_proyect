// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VMips32soc.h for the primary calling header

#include "VMips32soc_VGATextCard.h" // For This
#include "VMips32soc__Syms.h"

#include "verilated_dpi.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VMips32soc_VGATextCard) {
    VL_CELL (palRom, VMips32soc_RomColorPalette);
    VL_CELL (frameBuff, VMips32soc_DualPortVGARam);
    VL_CELL (fontRom, VMips32soc_FontRom);
    // Reset internal values
    // Reset structure values
    _ctor_var_reset();
}

void VMips32soc_VGATextCard::__Vconfigure(VMips32soc__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VMips32soc_VGATextCard::~VMips32soc_VGATextCard() {
}

//--------------------
// Internal Methods

void VMips32soc_VGATextCard::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("        VMips32soc_VGATextCard::_ctor_var_reset\n"); );
    // Body
    __PVT__vclk = VL_RAND_RESET_I(1);
    __PVT__clk = VL_RAND_RESET_I(1);
    __PVT__rst = VL_RAND_RESET_I(1);
    __PVT__en = VL_RAND_RESET_I(1);
    __PVT__we = VL_RAND_RESET_I(4);
    __PVT__addr = VL_RAND_RESET_I(11);
    __PVT__wd = VL_RAND_RESET_I(32);
    __PVT__rd = VL_RAND_RESET_I(32);
    __PVT__r = VL_RAND_RESET_I(3);
    __PVT__g = VL_RAND_RESET_I(3);
    __PVT__b = VL_RAND_RESET_I(2);
    __PVT__hs = VL_RAND_RESET_I(1);
    __PVT__vs = VL_RAND_RESET_I(1);
    __PVT__fontrom_addr = VL_RAND_RESET_I(12);
    __PVT__palrom_addr1 = VL_RAND_RESET_I(4);
}

void VMips32soc_VGATextCard::_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("        VMips32soc_VGATextCard::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
