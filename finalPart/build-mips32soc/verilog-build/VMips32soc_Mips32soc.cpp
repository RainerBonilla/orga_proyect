// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VMips32soc.h for the primary calling header

#include "VMips32soc_Mips32soc.h" // For This
#include "VMips32soc__Syms.h"

#include "verilated_dpi.h"

//*** Below code from `systemc in Verilog file
//#line 67 "/home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v"

 #include "Mips32SocSim.h" 
//*** Above code from `systemc in Verilog file


//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VMips32soc_Mips32soc) {
    VL_CELL (regFile, VMips32soc_RegFile);
    VL_CELL (vgaTc, VMips32soc_VGATextCard);
    VL_CELL (dataMem, VMips32soc_DataMem);
    VL_CELL (instMem, VMips32soc_InstMem);
    // Reset internal values
    // Reset structure values
    _ctor_var_reset();
}

void VMips32soc_Mips32soc::__Vconfigure(VMips32soc__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VMips32soc_Mips32soc::~VMips32soc_Mips32soc() {
}

//--------------------
// Internal Methods

void VMips32soc_Mips32soc::_settle__TOP__Mips32soc__1(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_settle__TOP__Mips32soc__1\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlSymsp->TOP__Mips32soc.__PVT__sam = (0x1fU & vlSymsp->TOP__Mips32soc.__PVT__sam);
}

VL_INLINE_OPT void VMips32soc_Mips32soc::_sequent__TOP__Mips32soc__2(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_sequent__TOP__Mips32soc__2\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlSymsp->TOP__Mips32soc.__Vdly__cclk = vlSymsp->TOP__Mips32soc.__PVT__cclk;
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:44
    vlSymsp->TOP__Mips32soc.__Vdly__cclk = (1U & (~ (IData)(vlSymsp->TOP__Mips32soc.__PVT__cclk)));
    vlSymsp->TOP__Mips32soc.__PVT__cclk = vlSymsp->TOP__Mips32soc.__Vdly__cclk;
}

void VMips32soc_Mips32soc::_initial__TOP__Mips32soc__3(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_initial__TOP__Mips32soc__3\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // INITIAL at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:47
    vlSymsp->TOP__Mips32soc.__PVT__cclk = 0U;
}

VL_INLINE_OPT void VMips32soc_Mips32soc::_sequent__TOP__Mips32soc__4(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_sequent__TOP__Mips32soc__4\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:76
    if (vlTOPp->rst) {
	vlSymsp->TOP__Mips32soc.__PVT__MsCounter_i2__DOT__ms_count 
	    = 
	// $c function at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:83
millis()	
	;
    } else {
	vlSymsp->TOP__Mips32soc.__PVT__MsCounter_i2__DOT__ms_count = 0U;
    }
}

VL_INLINE_OPT void VMips32soc_Mips32soc::_sequent__TOP__Mips32soc__5(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_sequent__TOP__Mips32soc__5\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:110
    if ((1U & ((~ (IData)(vlSymsp->TOP__Mips32soc.__PVT__err_temp)) 
	       | (~ (IData)(vlTOPp->rst))))) {
	vlSymsp->TOP__Mips32soc.__PVT__DIG_Register_BUS_i3__DOT__state 
	    = vlSymsp->TOP__Mips32soc.pc;
    }
}

VL_INLINE_OPT void VMips32soc_Mips32soc::_sequent__TOP__Mips32soc__6(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_sequent__TOP__Mips32soc__6\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlSymsp->TOP__Mips32soc.__PVT__s15 = ((0xffffU 
					   & vlSymsp->TOP__Mips32soc.__PVT__s15) 
					  | (0xffff0000U 
					     & (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
						<< 0x10U)));
    vlSymsp->TOP__Mips32soc.__PVT__sam = ((0xffffffe0U 
					   & vlSymsp->TOP__Mips32soc.__PVT__sam) 
					  | (0x1fU 
					     & (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
						>> 6U)));
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:630
    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
    if ((0x80000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
	if ((0x40000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
	    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
	    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
	    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
	    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
	} else {
	    if ((0x20000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
		if ((0x10000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
		    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
		    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
		    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
		    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
		} else {
		    if ((0x8000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
			}
		    } else {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__mds = 1U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__mds = 2U;
			}
		    }
		}
	    } else {
		if ((0x10000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
		    if ((0x8000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
			vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
			vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
		    } else {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__mds = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__szel = 0U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__mds = 2U;
			    vlSymsp->TOP__Mips32soc.__PVT__szel = 0U;
			}
		    }
		} else {
		    if ((0x8000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
			}
		    } else {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__mds = 1U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__mds = 2U;
			}
		    }
		}
	    }
	}
    } else {
	if ((0x40000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
	    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
	    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
	    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
	    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
	} else {
	    if ((0x20000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
		if ((0x10000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
		    if ((0x8000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 2U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 5U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__sze = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			}
		    } else {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 3U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__sze = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 2U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__sze = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			}
		    }
		} else {
		    if ((0x8000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 6U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 4U;
			}
		    } else {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			}
		    }
		}
	    } else {
		if ((0x10000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
		    if ((0x8000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 1U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__blez = 1U;
			}
		    } else {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bnes = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__beqs = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			}
		    }
		} else {
		    if ((0x8000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 2U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 3U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__jal = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__jump = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			}
		    } else {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    if ((1U == (0x1fU & (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
						 >> 0x10U)))) {
				vlSymsp->TOP__Mips32soc.__PVT__bgez = 1U;
				vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
			    } else {
				vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
				vlSymsp->TOP__Mips32soc.__PVT__bltz = 1U;
			    }
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			} else {
			    if ((0x20U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
				if ((0x10U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
				    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
				    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
				    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
				    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
				} else {
				    if ((8U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
					if ((4U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
					    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
					    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
					    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
					    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
					} else {
					    if ((2U 
						 & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 6U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 4U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						}
					    } else {
						vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
						vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
						vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
					    }
					}
				    } else {
					if ((4U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
					    if ((2U 
						 & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 5U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						}
					    } else {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 3U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 2U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						}
					    }
					} else {
					    if ((2U 
						 & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						}
					    } else {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						}
					    }
					}
				    }
				}
			    } else {
				if ((0x10U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
				    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
				    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
				    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
				    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
				} else {
				    if ((8U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
					if ((4U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
					    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
					    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
					    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
					    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
					} else {
					    if ((2U 
						 & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
						vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
						vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
					    } else {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jr = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						}
					    }
					}
				    } else {
					if ((4U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
					    if ((2U 
						 & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 9U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 3U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 1U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 8U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 3U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 1U;
						}
					    } else {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 7U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 3U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 1U;
						}
					    }
					} else {
					    if ((2U 
						 & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 9U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 2U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 1U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 8U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 2U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 1U;
						}
					    } else {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 7U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 2U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 1U;
						}
					    }
					}
				    }
				}
			    }
			}
		    }
		}
	    }
	}
    }
}

void VMips32soc_Mips32soc::_settle__TOP__Mips32soc__7(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_settle__TOP__Mips32soc__7\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlSymsp->TOP__Mips32soc.__PVT__s15 = (0xffff0000U 
					  & vlSymsp->TOP__Mips32soc.__PVT__s15);
    vlSymsp->TOP__Mips32soc.__PVT__s15 = ((0xffffU 
					   & vlSymsp->TOP__Mips32soc.__PVT__s15) 
					  | (0xffff0000U 
					     & (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
						<< 0x10U)));
    vlSymsp->TOP__Mips32soc.__PVT__sam = ((0xffffffe0U 
					   & vlSymsp->TOP__Mips32soc.__PVT__sam) 
					  | (0x1fU 
					     & (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
						>> 6U)));
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:630
    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
    if ((0x80000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
	if ((0x40000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
	    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
	    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
	    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
	    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
	} else {
	    if ((0x20000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
		if ((0x10000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
		    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
		    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
		    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
		    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
		} else {
		    if ((0x8000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
			}
		    } else {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__mds = 1U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__mds = 2U;
			}
		    }
		}
	    } else {
		if ((0x10000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
		    if ((0x8000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
			vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
			vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
		    } else {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__mds = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__szel = 0U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__mds = 2U;
			    vlSymsp->TOP__Mips32soc.__PVT__szel = 0U;
			}
		    }
		} else {
		    if ((0x8000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
			}
		    } else {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__mds = 1U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__mds = 2U;
			}
		    }
		}
	    }
	}
    } else {
	if ((0x40000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
	    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
	    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
	    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
	    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
	} else {
	    if ((0x20000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
		if ((0x10000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
		    if ((0x8000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 2U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 5U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__sze = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			}
		    } else {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 3U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__sze = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 2U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__sze = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			}
		    }
		} else {
		    if ((0x8000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 6U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 4U;
			}
		    } else {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			}
		    }
		}
	    } else {
		if ((0x10000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
		    if ((0x8000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 1U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__blez = 1U;
			}
		    } else {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__bnes = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__beqs = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			}
		    }
		} else {
		    if ((0x8000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 2U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 3U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__jal = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__jump = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			}
		    } else {
			if ((0x4000000U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
			    if ((1U == (0x1fU & (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
						 >> 0x10U)))) {
				vlSymsp->TOP__Mips32soc.__PVT__bgez = 1U;
				vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
			    } else {
				vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
				vlSymsp->TOP__Mips32soc.__PVT__bltz = 1U;
			    }
			    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 1U;
			    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
			    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
			} else {
			    if ((0x20U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
				if ((0x10U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
				    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
				    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
				    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
				    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
				} else {
				    if ((8U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
					if ((4U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
					    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
					    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
					    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
					    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
					} else {
					    if ((2U 
						 & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 6U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 4U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						}
					    } else {
						vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
						vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
						vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
					    }
					}
				    } else {
					if ((4U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
					    if ((2U 
						 & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 5U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						}
					    } else {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 3U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 2U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						}
					    }
					} else {
					    if ((2U 
						 & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						}
					    } else {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						}
					    }
					}
				    }
				}
			    } else {
				if ((0x10U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
				    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
				    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
				    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
				    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
				    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
				} else {
				    if ((8U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
					if ((4U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
					    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
					    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
					    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
					    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
					    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
					} else {
					    if ((2U 
						 & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
						vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
						vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
						vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
					    } else {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jr = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						}
					    }
					}
				    } else {
					if ((4U & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
					    if ((2U 
						 & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 9U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 3U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 1U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 8U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 3U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 1U;
						}
					    } else {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 7U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 3U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 1U;
						}
					    }
					} else {
					    if ((2U 
						 & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 9U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 2U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 1U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 8U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 2U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 1U;
						}
					    } else {
						if (
						    (1U 
						     & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)) {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jump = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__beqs = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bnes = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__sze = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__szel = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__mds = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__jr = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__jal = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__blez = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bltz = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bgez = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__bgtz = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 0U;
						} else {
						    vlSymsp->TOP__Mips32soc.__PVT__aluOp = 7U;
						    vlSymsp->TOP__Mips32soc.__PVT__regDest = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__aluSrc = 2U;
						    vlSymsp->TOP__Mips32soc.__PVT__memtoReg = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__regWrite = 1U;
						    vlSymsp->TOP__Mips32soc.__PVT__memRead = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__memWrite = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp = 0U;
						    vlSymsp->TOP__Mips32soc.__PVT__shift = 1U;
						}
					    }
					}
				    }
				}
			    }
			}
		    }
		}
	    }
	}
    }
    vlSymsp->TOP__Mips32soc.__PVT__jump_taken = (((IData)(vlSymsp->TOP__Mips32soc.__PVT__jr) 
						  << 1U) 
						 | ((IData)(vlSymsp->TOP__Mips32soc.__PVT__jump) 
						    | (IData)(vlSymsp->TOP__Mips32soc.__PVT__jal)));
}

void VMips32soc_Mips32soc::_settle__TOP__Mips32soc__8(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_settle__TOP__Mips32soc__8\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:151
    vlSymsp->TOP__Mips32soc.__PVT__s9 = ((IData)(vlSymsp->TOP__Mips32soc.__PVT__shift)
					  ? ((IData)(vlSymsp->TOP__Mips32soc.__PVT__shift)
					      ? vlSymsp->TOP__Mips32soc__regFile.__PVT__Db
					      : 0U)
					  : vlSymsp->TOP__Mips32soc__regFile.__PVT__Da);
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:1177
    vlSymsp->TOP__Mips32soc.__PVT__s10 = ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluSrc))
					   ? ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluSrc))
					       ? vlSymsp->TOP__Mips32soc__regFile.__PVT__Da
					       : vlSymsp->TOP__Mips32soc.__PVT__sam)
					   : ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluSrc))
					       ? ((IData)(vlSymsp->TOP__Mips32soc.__PVT__sze)
						   ? 
						  ((IData)(vlSymsp->TOP__Mips32soc.__PVT__sze)
						    ? 
						   (0xffffU 
						    & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)
						    : 0U)
						   : 
						  ((0xffff0000U 
						    & (VL_NEGATE_I((IData)(
									   (1U 
									    & (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
									       >> 0xfU)))) 
						       << 0x10U)) 
						   | (0xffffU 
						      & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)))
					       : vlSymsp->TOP__Mips32soc__regFile.__PVT__Db));
}

void VMips32soc_Mips32soc::_initial__TOP__Mips32soc__9(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_initial__TOP__Mips32soc__9\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // INITIAL at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:106
    vlSymsp->TOP__Mips32soc.__PVT__DIG_Register_BUS_i3__DOT__state = 0U;
}

VL_INLINE_OPT void VMips32soc_Mips32soc::_sequent__TOP__Mips32soc__10(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_sequent__TOP__Mips32soc__10\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlSymsp->TOP__Mips32soc.__PVT__jump_taken = (((IData)(vlSymsp->TOP__Mips32soc.__PVT__jr) 
						  << 1U) 
						 | ((IData)(vlSymsp->TOP__Mips32soc.__PVT__jump) 
						    | (IData)(vlSymsp->TOP__Mips32soc.__PVT__jal)));
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:151
    vlSymsp->TOP__Mips32soc.__PVT__s9 = ((IData)(vlSymsp->TOP__Mips32soc.__PVT__shift)
					  ? ((IData)(vlSymsp->TOP__Mips32soc.__PVT__shift)
					      ? vlSymsp->TOP__Mips32soc__regFile.__PVT__Db
					      : 0U)
					  : vlSymsp->TOP__Mips32soc__regFile.__PVT__Da);
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:1177
    vlSymsp->TOP__Mips32soc.__PVT__s10 = ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluSrc))
					   ? ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluSrc))
					       ? vlSymsp->TOP__Mips32soc__regFile.__PVT__Da
					       : vlSymsp->TOP__Mips32soc.__PVT__sam)
					   : ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluSrc))
					       ? ((IData)(vlSymsp->TOP__Mips32soc.__PVT__sze)
						   ? 
						  ((IData)(vlSymsp->TOP__Mips32soc.__PVT__sze)
						    ? 
						   (0xffffU 
						    & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)
						    : 0U)
						   : 
						  ((0xffff0000U 
						    & (VL_NEGATE_I((IData)(
									   (1U 
									    & (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
									       >> 0xfU)))) 
						       << 0x10U)) 
						   | (0xffffU 
						      & vlSymsp->TOP__Mips32soc__instMem.__PVT__dout)))
					       : vlSymsp->TOP__Mips32soc__regFile.__PVT__Db));
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:1209
    vlSymsp->TOP__Mips32soc.aRes = ((8U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
				     ? ((4U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
					 ? 0U : ((2U 
						  & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
						  ? 0U
						  : 
						 ((1U 
						   & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
						   ? 
						  ((0x1fU 
						    >= vlSymsp->TOP__Mips32soc.__PVT__s10)
						    ? 
						   VL_SHIFTRS_III(32,32,32, vlSymsp->TOP__Mips32soc.__PVT__s9, vlSymsp->TOP__Mips32soc.__PVT__s10)
						    : 
						   VL_NEGATE_I(
							       (vlSymsp->TOP__Mips32soc.__PVT__s9 
								>> 0x1fU)))
						   : 
						  ((0x1fU 
						    >= vlSymsp->TOP__Mips32soc.__PVT__s10)
						    ? 
						   (vlSymsp->TOP__Mips32soc.__PVT__s9 
						    >> vlSymsp->TOP__Mips32soc.__PVT__s10)
						    : 0U))))
				     : ((4U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
					 ? ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
					     ? ((1U 
						 & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
						 ? 
						((0x1fU 
						  >= vlSymsp->TOP__Mips32soc.__PVT__s10)
						  ? 
						 (vlSymsp->TOP__Mips32soc.__PVT__s9 
						  << vlSymsp->TOP__Mips32soc.__PVT__s10)
						  : 0U)
						 : 
						(vlSymsp->TOP__Mips32soc.__PVT__s9 
						 < vlSymsp->TOP__Mips32soc.__PVT__s10))
					     : ((1U 
						 & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
						 ? 
						(vlSymsp->TOP__Mips32soc.__PVT__s9 
						 ^ vlSymsp->TOP__Mips32soc.__PVT__s10)
						 : 
						VL_LTS_III(32,32,32, vlSymsp->TOP__Mips32soc.__PVT__s9, vlSymsp->TOP__Mips32soc.__PVT__s10)))
					 : ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
					     ? ((1U 
						 & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
						 ? 
						(vlSymsp->TOP__Mips32soc.__PVT__s9 
						 | vlSymsp->TOP__Mips32soc.__PVT__s10)
						 : 
						(vlSymsp->TOP__Mips32soc.__PVT__s9 
						 & vlSymsp->TOP__Mips32soc.__PVT__s10))
					     : ((1U 
						 & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
						 ? 
						(vlSymsp->TOP__Mips32soc.__PVT__s9 
						 - vlSymsp->TOP__Mips32soc.__PVT__s10)
						 : 
						(vlSymsp->TOP__Mips32soc.__PVT__s9 
						 + vlSymsp->TOP__Mips32soc.__PVT__s10)))));
}

void VMips32soc_Mips32soc::_settle__TOP__Mips32soc__11(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_settle__TOP__Mips32soc__11\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:1209
    vlSymsp->TOP__Mips32soc.aRes = ((8U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
				     ? ((4U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
					 ? 0U : ((2U 
						  & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
						  ? 0U
						  : 
						 ((1U 
						   & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
						   ? 
						  ((0x1fU 
						    >= vlSymsp->TOP__Mips32soc.__PVT__s10)
						    ? 
						   VL_SHIFTRS_III(32,32,32, vlSymsp->TOP__Mips32soc.__PVT__s9, vlSymsp->TOP__Mips32soc.__PVT__s10)
						    : 
						   VL_NEGATE_I(
							       (vlSymsp->TOP__Mips32soc.__PVT__s9 
								>> 0x1fU)))
						   : 
						  ((0x1fU 
						    >= vlSymsp->TOP__Mips32soc.__PVT__s10)
						    ? 
						   (vlSymsp->TOP__Mips32soc.__PVT__s9 
						    >> vlSymsp->TOP__Mips32soc.__PVT__s10)
						    : 0U))))
				     : ((4U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
					 ? ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
					     ? ((1U 
						 & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
						 ? 
						((0x1fU 
						  >= vlSymsp->TOP__Mips32soc.__PVT__s10)
						  ? 
						 (vlSymsp->TOP__Mips32soc.__PVT__s9 
						  << vlSymsp->TOP__Mips32soc.__PVT__s10)
						  : 0U)
						 : 
						(vlSymsp->TOP__Mips32soc.__PVT__s9 
						 < vlSymsp->TOP__Mips32soc.__PVT__s10))
					     : ((1U 
						 & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
						 ? 
						(vlSymsp->TOP__Mips32soc.__PVT__s9 
						 ^ vlSymsp->TOP__Mips32soc.__PVT__s10)
						 : 
						VL_LTS_III(32,32,32, vlSymsp->TOP__Mips32soc.__PVT__s9, vlSymsp->TOP__Mips32soc.__PVT__s10)))
					 : ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
					     ? ((1U 
						 & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
						 ? 
						(vlSymsp->TOP__Mips32soc.__PVT__s9 
						 | vlSymsp->TOP__Mips32soc.__PVT__s10)
						 : 
						(vlSymsp->TOP__Mips32soc.__PVT__s9 
						 & vlSymsp->TOP__Mips32soc.__PVT__s10))
					     : ((1U 
						 & (IData)(vlSymsp->TOP__Mips32soc.__PVT__aluOp))
						 ? 
						(vlSymsp->TOP__Mips32soc.__PVT__s9 
						 - vlSymsp->TOP__Mips32soc.__PVT__s10)
						 : 
						(vlSymsp->TOP__Mips32soc.__PVT__s9 
						 + vlSymsp->TOP__Mips32soc.__PVT__s10)))));
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:1237
    if (((IData)(vlSymsp->TOP__Mips32soc.__PVT__memRead) 
	 | (IData)(vlSymsp->TOP__Mips32soc.__PVT__memWrite))) {
	if (((0x10010000U <= vlSymsp->TOP__Mips32soc.aRes) 
	     & (0x10011000U > vlSymsp->TOP__Mips32soc.aRes))) {
	    vlSymsp->TOP__Mips32soc.__PVT__s16 = (0x1fffU 
						  & vlSymsp->TOP__Mips32soc.aRes);
	    vlSymsp->TOP__Mips32soc.__PVT__iAddr_temp = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__mb = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__me = (((IData)(vlSymsp->TOP__Mips32soc.__PVT__memRead) 
						  | (IData)(vlSymsp->TOP__Mips32soc.__PVT__memWrite))
						  ? 1U
						  : 0U);
	} else {
	    if (((0x7fffeffcU <= vlSymsp->TOP__Mips32soc.aRes) 
		 & (0x7ffffffcU > vlSymsp->TOP__Mips32soc.aRes))) {
		vlSymsp->TOP__Mips32soc.__PVT__s16 
		    = (0x1fffU & ((IData)(0x1000U) 
				  + (vlSymsp->TOP__Mips32soc.aRes 
				     - (IData)(0xffcU))));
		vlSymsp->TOP__Mips32soc.__PVT__iAddr_temp = 0U;
		vlSymsp->TOP__Mips32soc.__PVT__mb = 0U;
		vlSymsp->TOP__Mips32soc.__PVT__me = 
		    (((IData)(vlSymsp->TOP__Mips32soc.__PVT__memRead) 
		      | (IData)(vlSymsp->TOP__Mips32soc.__PVT__memWrite))
		      ? 1U : 0U);
	    } else {
		if (((0xb800U <= vlSymsp->TOP__Mips32soc.aRes) 
		     & (0xcabfU >= vlSymsp->TOP__Mips32soc.aRes))) {
		    vlSymsp->TOP__Mips32soc.__PVT__s16 
			= (0x1fffU & (vlSymsp->TOP__Mips32soc.aRes 
				      - (IData)(0x1800U)));
		    vlSymsp->TOP__Mips32soc.__PVT__iAddr_temp = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__mb = 1U;
		    vlSymsp->TOP__Mips32soc.__PVT__me 
			= (((IData)(vlSymsp->TOP__Mips32soc.__PVT__memWrite) 
			    | (IData)(vlSymsp->TOP__Mips32soc.__PVT__memRead))
			    ? 2U : 0U);
		} else {
		    if (((0xffff0000U <= vlSymsp->TOP__Mips32soc.aRes) 
			 & (0xffff000fU >= vlSymsp->TOP__Mips32soc.aRes))) {
			vlSymsp->TOP__Mips32soc.__PVT__s16 
			    = (0x1fffU & vlSymsp->TOP__Mips32soc.aRes);
			vlSymsp->TOP__Mips32soc.__PVT__iAddr_temp = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__mb = 2U;
			vlSymsp->TOP__Mips32soc.__PVT__me 
			    = (((IData)(vlSymsp->TOP__Mips32soc.__PVT__memWrite) 
				| (IData)(vlSymsp->TOP__Mips32soc.__PVT__memRead))
			        ? 4U : 0U);
		    } else {
			vlSymsp->TOP__Mips32soc.__PVT__s16 = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__iAddr_temp 
			    = ((IData)(vlSymsp->TOP__Mips32soc.__PVT__memWrite) 
			       | (IData)(vlSymsp->TOP__Mips32soc.__PVT__memRead));
			vlSymsp->TOP__Mips32soc.__PVT__mb = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__me = 0U;
		    }
		}
	    }
	}
    } else {
	vlSymsp->TOP__Mips32soc.__PVT__mb = 0U;
	vlSymsp->TOP__Mips32soc.__PVT__me = 0U;
	vlSymsp->TOP__Mips32soc.__PVT__iAddr_temp = 0U;
    }
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:1291
    vlSymsp->TOP__Mips32soc.__PVT__branch_taken = (1U 
						   & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__beqs)
						       ? 
						      (0U 
						       == vlSymsp->TOP__Mips32soc.aRes)
						       : 
						      ((IData)(vlSymsp->TOP__Mips32soc.__PVT__bnes)
						        ? 
						       (0U 
							!= vlSymsp->TOP__Mips32soc.aRes)
						        : 
						       ((IData)(vlSymsp->TOP__Mips32soc.__PVT__bgez)
							 ? 
							(~ 
							 (vlSymsp->TOP__Mips32soc.aRes 
							  >> 0x1fU))
							 : 
							((IData)(vlSymsp->TOP__Mips32soc.__PVT__bgtz)
							  ? 
							 ((0U 
							   != vlSymsp->TOP__Mips32soc.aRes) 
							  & (~ 
							     (vlSymsp->TOP__Mips32soc.aRes 
							      >> 0x1fU)))
							  : 
							 ((IData)(vlSymsp->TOP__Mips32soc.__PVT__blez)
							   ? 
							  ((0U 
							    == vlSymsp->TOP__Mips32soc.aRes) 
							   | (vlSymsp->TOP__Mips32soc.aRes 
							      >> 0x1fU))
							   : 
							  ((IData)(vlSymsp->TOP__Mips32soc.__PVT__bltz) 
							   & (vlSymsp->TOP__Mips32soc.aRes 
							      >> 0x1fU))))))));
}

VL_INLINE_OPT void VMips32soc_Mips32soc::_sequent__TOP__Mips32soc__12(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_sequent__TOP__Mips32soc__12\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:1237
    if (((IData)(vlSymsp->TOP__Mips32soc.__PVT__memRead) 
	 | (IData)(vlSymsp->TOP__Mips32soc.__PVT__memWrite))) {
	if (((0x10010000U <= vlSymsp->TOP__Mips32soc.aRes) 
	     & (0x10011000U > vlSymsp->TOP__Mips32soc.aRes))) {
	    vlSymsp->TOP__Mips32soc.__PVT__s16 = (0x1fffU 
						  & vlSymsp->TOP__Mips32soc.aRes);
	    vlSymsp->TOP__Mips32soc.__PVT__iAddr_temp = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__mb = 0U;
	    vlSymsp->TOP__Mips32soc.__PVT__me = (((IData)(vlSymsp->TOP__Mips32soc.__PVT__memRead) 
						  | (IData)(vlSymsp->TOP__Mips32soc.__PVT__memWrite))
						  ? 1U
						  : 0U);
	} else {
	    if (((0x7fffeffcU <= vlSymsp->TOP__Mips32soc.aRes) 
		 & (0x7ffffffcU > vlSymsp->TOP__Mips32soc.aRes))) {
		vlSymsp->TOP__Mips32soc.__PVT__s16 
		    = (0x1fffU & ((IData)(0x1000U) 
				  + (vlSymsp->TOP__Mips32soc.aRes 
				     - (IData)(0xffcU))));
		vlSymsp->TOP__Mips32soc.__PVT__iAddr_temp = 0U;
		vlSymsp->TOP__Mips32soc.__PVT__mb = 0U;
		vlSymsp->TOP__Mips32soc.__PVT__me = 
		    (((IData)(vlSymsp->TOP__Mips32soc.__PVT__memRead) 
		      | (IData)(vlSymsp->TOP__Mips32soc.__PVT__memWrite))
		      ? 1U : 0U);
	    } else {
		if (((0xb800U <= vlSymsp->TOP__Mips32soc.aRes) 
		     & (0xcabfU >= vlSymsp->TOP__Mips32soc.aRes))) {
		    vlSymsp->TOP__Mips32soc.__PVT__s16 
			= (0x1fffU & (vlSymsp->TOP__Mips32soc.aRes 
				      - (IData)(0x1800U)));
		    vlSymsp->TOP__Mips32soc.__PVT__iAddr_temp = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__mb = 1U;
		    vlSymsp->TOP__Mips32soc.__PVT__me 
			= (((IData)(vlSymsp->TOP__Mips32soc.__PVT__memWrite) 
			    | (IData)(vlSymsp->TOP__Mips32soc.__PVT__memRead))
			    ? 2U : 0U);
		} else {
		    if (((0xffff0000U <= vlSymsp->TOP__Mips32soc.aRes) 
			 & (0xffff000fU >= vlSymsp->TOP__Mips32soc.aRes))) {
			vlSymsp->TOP__Mips32soc.__PVT__s16 
			    = (0x1fffU & vlSymsp->TOP__Mips32soc.aRes);
			vlSymsp->TOP__Mips32soc.__PVT__iAddr_temp = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__mb = 2U;
			vlSymsp->TOP__Mips32soc.__PVT__me 
			    = (((IData)(vlSymsp->TOP__Mips32soc.__PVT__memWrite) 
				| (IData)(vlSymsp->TOP__Mips32soc.__PVT__memRead))
			        ? 4U : 0U);
		    } else {
			vlSymsp->TOP__Mips32soc.__PVT__s16 = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__iAddr_temp 
			    = ((IData)(vlSymsp->TOP__Mips32soc.__PVT__memWrite) 
			       | (IData)(vlSymsp->TOP__Mips32soc.__PVT__memRead));
			vlSymsp->TOP__Mips32soc.__PVT__mb = 0U;
			vlSymsp->TOP__Mips32soc.__PVT__me = 0U;
		    }
		}
	    }
	}
    } else {
	vlSymsp->TOP__Mips32soc.__PVT__mb = 0U;
	vlSymsp->TOP__Mips32soc.__PVT__me = 0U;
	vlSymsp->TOP__Mips32soc.__PVT__iAddr_temp = 0U;
    }
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:1291
    vlSymsp->TOP__Mips32soc.__PVT__branch_taken = (1U 
						   & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__beqs)
						       ? 
						      (0U 
						       == vlSymsp->TOP__Mips32soc.aRes)
						       : 
						      ((IData)(vlSymsp->TOP__Mips32soc.__PVT__bnes)
						        ? 
						       (0U 
							!= vlSymsp->TOP__Mips32soc.aRes)
						        : 
						       ((IData)(vlSymsp->TOP__Mips32soc.__PVT__bgez)
							 ? 
							(~ 
							 (vlSymsp->TOP__Mips32soc.aRes 
							  >> 0x1fU))
							 : 
							((IData)(vlSymsp->TOP__Mips32soc.__PVT__bgtz)
							  ? 
							 ((0U 
							   != vlSymsp->TOP__Mips32soc.aRes) 
							  & (~ 
							     (vlSymsp->TOP__Mips32soc.aRes 
							      >> 0x1fU)))
							  : 
							 ((IData)(vlSymsp->TOP__Mips32soc.__PVT__blez)
							   ? 
							  ((0U 
							    == vlSymsp->TOP__Mips32soc.aRes) 
							   | (vlSymsp->TOP__Mips32soc.aRes 
							      >> 0x1fU))
							   : 
							  ((IData)(vlSymsp->TOP__Mips32soc.__PVT__bltz) 
							   & (vlSymsp->TOP__Mips32soc.aRes 
							      >> 0x1fU))))))));
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:1343
    if (vlSymsp->TOP__Mips32soc.__PVT__memWrite) {
	vlSymsp->TOP__Mips32soc.__PVT__outdata = 0U;
	vlSymsp->TOP__Mips32soc.__PVT__outwe = 0U;
	if ((0U == (IData)(vlSymsp->TOP__Mips32soc.__PVT__mds))) {
	    vlSymsp->TOP__Mips32soc.__PVT__outdata 
		= vlSymsp->TOP__Mips32soc__regFile.__PVT__Db;
	    vlSymsp->TOP__Mips32soc.__PVT__outwe = 0xfU;
	} else {
	    if ((1U == (IData)(vlSymsp->TOP__Mips32soc.__PVT__mds))) {
		if ((1U >= (3U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__s16)))) {
		    vlSymsp->TOP__Mips32soc.__PVT__outdata 
			= (0xffff0000U & (vlSymsp->TOP__Mips32soc__regFile.__PVT__Db 
					  << 0x10U));
		    vlSymsp->TOP__Mips32soc.__PVT__outwe = 3U;
		} else {
		    vlSymsp->TOP__Mips32soc.__PVT__outdata 
			= (0xffffU & vlSymsp->TOP__Mips32soc__regFile.__PVT__Db);
		    vlSymsp->TOP__Mips32soc.__PVT__outwe = 0xcU;
		}
	    } else {
		if ((2U == (IData)(vlSymsp->TOP__Mips32soc.__PVT__mds))) {
		    if ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__s16))) {
			if ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__s16))) {
			    vlSymsp->TOP__Mips32soc.__PVT__outdata 
				= (0xffU & vlSymsp->TOP__Mips32soc__regFile.__PVT__Db);
			    vlSymsp->TOP__Mips32soc.__PVT__outwe = 8U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__outdata 
				= (0xff00U & (vlSymsp->TOP__Mips32soc__regFile.__PVT__Db 
					      << 8U));
			    vlSymsp->TOP__Mips32soc.__PVT__outwe = 4U;
			}
		    } else {
			if ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__s16))) {
			    vlSymsp->TOP__Mips32soc.__PVT__outdata 
				= (0xff0000U & (vlSymsp->TOP__Mips32soc__regFile.__PVT__Db 
						<< 0x10U));
			    vlSymsp->TOP__Mips32soc.__PVT__outwe = 2U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__outdata 
				= (0xff000000U & (vlSymsp->TOP__Mips32soc__regFile.__PVT__Db 
						  << 0x18U));
			    vlSymsp->TOP__Mips32soc.__PVT__outwe = 1U;
			}
		    }
		} else {
		    vlSymsp->TOP__Mips32soc.__PVT__outdata = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__outwe = 0U;
		}
	    }
	}
    } else {
	vlSymsp->TOP__Mips32soc.__PVT__outdata = 0U;
	vlSymsp->TOP__Mips32soc.__PVT__outwe = 0U;
    }
}

VL_INLINE_OPT void VMips32soc_Mips32soc::_combo__TOP__Mips32soc__13(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_combo__TOP__Mips32soc__13\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:1177
    vlSymsp->TOP__Mips32soc.__PVT__s19 = ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__mb))
					   ? ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__mb))
					       ? 0U
					       : ((4U 
						   & (IData)(vlSymsp->TOP__Mips32soc.__PVT__me))
						   ? 
						  ((1U 
						    == 
						    (0x7ffU 
						     & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__s16) 
							>> 2U)))
						    ? 
						   ((IData)(vlTOPp->keypad) 
						    << 0x18U)
						    : 
						   ((2U 
						     == 
						     (0x7ffU 
						      & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__s16) 
							 >> 2U)))
						     ? vlSymsp->TOP__Mips32soc.__PVT__MsCounter_i2__DOT__ms_count
						     : 0U))
						   : 0U))
					   : ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__mb))
					       ? vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__PVT__rda
					       : vlSymsp->TOP__Mips32soc__dataMem.__PVT__rd));
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:151
    vlSymsp->TOP__Mips32soc.pc = ((IData)(vlTOPp->rst)
				   ? ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__jump_taken))
				       ? ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__jump_taken))
					   ? 0U : vlSymsp->TOP__Mips32soc__regFile.__PVT__Da)
				       : ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__jump_taken))
					   ? ((0xf0000000U 
					       & ((IData)(
							  (VL_ULL(0x1f) 
							   & ((VL_ULL(4) 
							       + (QData)((IData)(vlSymsp->TOP__Mips32soc.__PVT__DIG_Register_BUS_i3__DOT__state))) 
							      >> 0x1cU))) 
						  << 0x1cU)) 
					      | (0xffffffcU 
						 & (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
						    << 2U)))
					   : ((IData)(vlSymsp->TOP__Mips32soc.__PVT__branch_taken)
					       ? ((IData)(vlSymsp->TOP__Mips32soc.__PVT__branch_taken)
						   ? 
						  ((IData)(4U) 
						   + 
						   (((0xfffc0000U 
						      & (VL_NEGATE_I((IData)(
									     (1U 
									      & (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
										>> 0xfU)))) 
							 << 0x12U)) 
						     | (0x3fffcU 
							& (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
							   << 2U))) 
						    + vlSymsp->TOP__Mips32soc.__PVT__DIG_Register_BUS_i3__DOT__state))
						   : 0U)
					       : ((IData)(4U) 
						  + vlSymsp->TOP__Mips32soc.__PVT__DIG_Register_BUS_i3__DOT__state))))
				   : ((IData)(vlTOPp->rst)
				       ? 0U : 0x400000U));
}

void VMips32soc_Mips32soc::_settle__TOP__Mips32soc__14(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_settle__TOP__Mips32soc__14\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:1177
    vlSymsp->TOP__Mips32soc.__PVT__s19 = ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__mb))
					   ? ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__mb))
					       ? 0U
					       : ((4U 
						   & (IData)(vlSymsp->TOP__Mips32soc.__PVT__me))
						   ? 
						  ((1U 
						    == 
						    (0x7ffU 
						     & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__s16) 
							>> 2U)))
						    ? 
						   ((IData)(vlTOPp->keypad) 
						    << 0x18U)
						    : 
						   ((2U 
						     == 
						     (0x7ffU 
						      & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__s16) 
							 >> 2U)))
						     ? vlSymsp->TOP__Mips32soc.__PVT__MsCounter_i2__DOT__ms_count
						     : 0U))
						   : 0U))
					   : ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__mb))
					       ? vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__PVT__rda
					       : vlSymsp->TOP__Mips32soc__dataMem.__PVT__rd));
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:1343
    if (vlSymsp->TOP__Mips32soc.__PVT__memWrite) {
	vlSymsp->TOP__Mips32soc.__PVT__outdata = 0U;
	vlSymsp->TOP__Mips32soc.__PVT__outwe = 0U;
	if ((0U == (IData)(vlSymsp->TOP__Mips32soc.__PVT__mds))) {
	    vlSymsp->TOP__Mips32soc.__PVT__outdata 
		= vlSymsp->TOP__Mips32soc__regFile.__PVT__Db;
	    vlSymsp->TOP__Mips32soc.__PVT__outwe = 0xfU;
	} else {
	    if ((1U == (IData)(vlSymsp->TOP__Mips32soc.__PVT__mds))) {
		if ((1U >= (3U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__s16)))) {
		    vlSymsp->TOP__Mips32soc.__PVT__outdata 
			= (0xffff0000U & (vlSymsp->TOP__Mips32soc__regFile.__PVT__Db 
					  << 0x10U));
		    vlSymsp->TOP__Mips32soc.__PVT__outwe = 3U;
		} else {
		    vlSymsp->TOP__Mips32soc.__PVT__outdata 
			= (0xffffU & vlSymsp->TOP__Mips32soc__regFile.__PVT__Db);
		    vlSymsp->TOP__Mips32soc.__PVT__outwe = 0xcU;
		}
	    } else {
		if ((2U == (IData)(vlSymsp->TOP__Mips32soc.__PVT__mds))) {
		    if ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__s16))) {
			if ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__s16))) {
			    vlSymsp->TOP__Mips32soc.__PVT__outdata 
				= (0xffU & vlSymsp->TOP__Mips32soc__regFile.__PVT__Db);
			    vlSymsp->TOP__Mips32soc.__PVT__outwe = 8U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__outdata 
				= (0xff00U & (vlSymsp->TOP__Mips32soc__regFile.__PVT__Db 
					      << 8U));
			    vlSymsp->TOP__Mips32soc.__PVT__outwe = 4U;
			}
		    } else {
			if ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__s16))) {
			    vlSymsp->TOP__Mips32soc.__PVT__outdata 
				= (0xff0000U & (vlSymsp->TOP__Mips32soc__regFile.__PVT__Db 
						<< 0x10U));
			    vlSymsp->TOP__Mips32soc.__PVT__outwe = 2U;
			} else {
			    vlSymsp->TOP__Mips32soc.__PVT__outdata 
				= (0xff000000U & (vlSymsp->TOP__Mips32soc__regFile.__PVT__Db 
						  << 0x18U));
			    vlSymsp->TOP__Mips32soc.__PVT__outwe = 1U;
			}
		    }
		} else {
		    vlSymsp->TOP__Mips32soc.__PVT__outdata = 0U;
		    vlSymsp->TOP__Mips32soc.__PVT__outwe = 0U;
		}
	    }
	}
    } else {
	vlSymsp->TOP__Mips32soc.__PVT__outdata = 0U;
	vlSymsp->TOP__Mips32soc.__PVT__outwe = 0U;
    }
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:151
    vlSymsp->TOP__Mips32soc.pc = ((IData)(vlTOPp->rst)
				   ? ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__jump_taken))
				       ? ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__jump_taken))
					   ? 0U : vlSymsp->TOP__Mips32soc__regFile.__PVT__Da)
				       : ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__jump_taken))
					   ? ((0xf0000000U 
					       & ((IData)(
							  (VL_ULL(0x1f) 
							   & ((VL_ULL(4) 
							       + (QData)((IData)(vlSymsp->TOP__Mips32soc.__PVT__DIG_Register_BUS_i3__DOT__state))) 
							      >> 0x1cU))) 
						  << 0x1cU)) 
					      | (0xffffffcU 
						 & (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
						    << 2U)))
					   : ((IData)(vlSymsp->TOP__Mips32soc.__PVT__branch_taken)
					       ? ((IData)(vlSymsp->TOP__Mips32soc.__PVT__branch_taken)
						   ? 
						  ((IData)(4U) 
						   + 
						   (((0xfffc0000U 
						      & (VL_NEGATE_I((IData)(
									     (1U 
									      & (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
										>> 0xfU)))) 
							 << 0x12U)) 
						     | (0x3fffcU 
							& (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
							   << 2U))) 
						    + vlSymsp->TOP__Mips32soc.__PVT__DIG_Register_BUS_i3__DOT__state))
						   : 0U)
					       : ((IData)(4U) 
						  + vlSymsp->TOP__Mips32soc.__PVT__DIG_Register_BUS_i3__DOT__state))))
				   : ((IData)(vlTOPp->rst)
				       ? 0U : 0x400000U));
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:553
    if (((0x400000U <= vlSymsp->TOP__Mips32soc.pc) 
	 & (0x402000U > vlSymsp->TOP__Mips32soc.pc))) {
	vlSymsp->TOP__Mips32soc.__PVT__s1 = (0x1fffU 
					     & vlSymsp->TOP__Mips32soc.pc);
	vlSymsp->TOP__Mips32soc.__PVT__iPC_temp = 0U;
    } else {
	vlSymsp->TOP__Mips32soc.__PVT__s1 = 0U;
	vlSymsp->TOP__Mips32soc.__PVT__iPC_temp = 1U;
    }
}

VL_INLINE_OPT void VMips32soc_Mips32soc::_combo__TOP__Mips32soc__15(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_combo__TOP__Mips32soc__15\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:553
    if (((0x400000U <= vlSymsp->TOP__Mips32soc.pc) 
	 & (0x402000U > vlSymsp->TOP__Mips32soc.pc))) {
	vlSymsp->TOP__Mips32soc.__PVT__s1 = (0x1fffU 
					     & vlSymsp->TOP__Mips32soc.pc);
	vlSymsp->TOP__Mips32soc.__PVT__iPC_temp = 0U;
    } else {
	vlSymsp->TOP__Mips32soc.__PVT__s1 = 0U;
	vlSymsp->TOP__Mips32soc.__PVT__iPC_temp = 1U;
    }
    vlSymsp->TOP__Mips32soc.__PVT__err_temp = (((IData)(vlSymsp->TOP__Mips32soc.__PVT__iPC_temp) 
						| (IData)(vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp)) 
					       | (IData)(vlSymsp->TOP__Mips32soc.__PVT__iAddr_temp));
}

void VMips32soc_Mips32soc::_settle__TOP__Mips32soc__16(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_settle__TOP__Mips32soc__16\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlSymsp->TOP__Mips32soc.__PVT__err_temp = (((IData)(vlSymsp->TOP__Mips32soc.__PVT__iPC_temp) 
						| (IData)(vlSymsp->TOP__Mips32soc.__PVT__iOpc_temp)) 
					       | (IData)(vlSymsp->TOP__Mips32soc.__PVT__iAddr_temp));
}

void VMips32soc_Mips32soc::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_ctor_var_reset\n"); );
    // Body
    fclk = VL_RAND_RESET_I(1);
    rst = VL_RAND_RESET_I(1);
    keypad = VL_RAND_RESET_I(8);
    green = VL_RAND_RESET_I(3);
    blue = VL_RAND_RESET_I(2);
    hsync = VL_RAND_RESET_I(1);
    vsync = VL_RAND_RESET_I(1);
    red = VL_RAND_RESET_I(3);
    iAddr = VL_RAND_RESET_I(1);
    iOpc = VL_RAND_RESET_I(1);
    iPC = VL_RAND_RESET_I(1);
    err = VL_RAND_RESET_I(1);
    pc = VL_RAND_RESET_I(32);
    __PVT__cclk = VL_RAND_RESET_I(1);
    __PVT__s1 = VL_RAND_RESET_I(13);
    __PVT__jump_taken = VL_RAND_RESET_I(2);
    __PVT__branch_taken = VL_RAND_RESET_I(1);
    __PVT__s9 = VL_RAND_RESET_I(32);
    __PVT__s10 = VL_RAND_RESET_I(32);
    __PVT__aluOp = VL_RAND_RESET_I(4);
    aRes = VL_RAND_RESET_I(32);
    __PVT__aluSrc = VL_RAND_RESET_I(2);
    __PVT__sam = VL_RAND_RESET_I(32);
    __PVT__regDest = VL_RAND_RESET_I(2);
    __PVT__memtoReg = VL_RAND_RESET_I(2);
    __PVT__s15 = VL_RAND_RESET_I(32);
    __PVT__jump = VL_RAND_RESET_I(1);
    __PVT__beqs = VL_RAND_RESET_I(1);
    __PVT__memRead = VL_RAND_RESET_I(1);
    __PVT__memWrite = VL_RAND_RESET_I(1);
    __PVT__regWrite = VL_RAND_RESET_I(1);
    __PVT__bnes = VL_RAND_RESET_I(1);
    __PVT__sze = VL_RAND_RESET_I(1);
    __PVT__mds = VL_RAND_RESET_I(2);
    __PVT__iOpc_temp = VL_RAND_RESET_I(1);
    __PVT__szel = VL_RAND_RESET_I(1);
    __PVT__jr = VL_RAND_RESET_I(1);
    __PVT__jal = VL_RAND_RESET_I(1);
    __PVT__blez = VL_RAND_RESET_I(1);
    __PVT__bltz = VL_RAND_RESET_I(1);
    __PVT__bgez = VL_RAND_RESET_I(1);
    __PVT__bgtz = VL_RAND_RESET_I(1);
    __PVT__shift = VL_RAND_RESET_I(1);
    __PVT__iPC_temp = VL_RAND_RESET_I(1);
    __PVT__s16 = VL_RAND_RESET_I(13);
    __PVT__iAddr_temp = VL_RAND_RESET_I(1);
    __PVT__me = VL_RAND_RESET_I(3);
    __PVT__mb = VL_RAND_RESET_I(2);
    __PVT__err_temp = VL_RAND_RESET_I(1);
    __PVT__outdata = VL_RAND_RESET_I(32);
    __PVT__outwe = VL_RAND_RESET_I(4);
    __PVT__s19 = VL_RAND_RESET_I(32);
    __PVT__MsCounter_i2__DOT__ms_count = VL_RAND_RESET_I(32);
    __PVT__DIG_Register_BUS_i3__DOT__state = VL_RAND_RESET_I(32);
    __Vdly__cclk = VL_RAND_RESET_I(1);
}

void VMips32soc_Mips32soc::_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("      VMips32soc_Mips32soc::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
