// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table internal header
//
// Internal details; most calling programs do not need this header

#ifndef _VMips32soc__Syms_H_
#define _VMips32soc__Syms_H_

#include "verilated.h"

// INCLUDE MODULE CLASSES
#include "VMips32soc.h"
#include "VMips32soc_Mips32soc.h"
#include "VMips32soc_RegFile.h"
#include "VMips32soc_VGATextCard.h"
#include "VMips32soc_DataMem.h"
#include "VMips32soc_InstMem.h"
#include "VMips32soc_DualPortVGARam.h"
#include "VMips32soc_FontRom.h"
#include "VMips32soc_RomColorPalette.h"

// DPI TYPES for DPI Export callbacks (Internal use)

// SYMS CLASS
class VMips32soc__Syms : public VerilatedSyms {
  public:
    
    // LOCAL STATE
    const char* __Vm_namep;
    bool	__Vm_activity;		///< Used by trace routines to determine change occurred
    bool	__Vm_didInit;
    //char	__VpadToAlign10[6];
    
    // SUBCELL STATE
    VMips32soc*                    TOPp;
    VMips32soc_Mips32soc           TOP__Mips32soc;
    VMips32soc_DataMem             TOP__Mips32soc__dataMem;
    VMips32soc_InstMem             TOP__Mips32soc__instMem;
    VMips32soc_RegFile             TOP__Mips32soc__regFile;
    VMips32soc_VGATextCard         TOP__Mips32soc__vgaTc;
    VMips32soc_FontRom             TOP__Mips32soc__vgaTc__fontRom;
    VMips32soc_DualPortVGARam      TOP__Mips32soc__vgaTc__frameBuff;
    VMips32soc_RomColorPalette     TOP__Mips32soc__vgaTc__palRom;
    
    // COVERAGE
    
    // SCOPE NAMES
    VerilatedScope __Vscope_Mips32soc;
    VerilatedScope __Vscope_Mips32soc__dataMem;
    VerilatedScope __Vscope_Mips32soc__instMem;
    VerilatedScope __Vscope_Mips32soc__regFile;
    VerilatedScope __Vscope_Mips32soc__vgaTc__fontRom;
    VerilatedScope __Vscope_Mips32soc__vgaTc__frameBuff;
    VerilatedScope __Vscope_Mips32soc__vgaTc__palRom;
    
    // CREATORS
    VMips32soc__Syms(VMips32soc* topp, const char* namep);
    ~VMips32soc__Syms() {};
    
    // METHODS
    inline const char* name() { return __Vm_namep; }
    inline bool getClearActivity() { bool r=__Vm_activity; __Vm_activity=false; return r;}
    
} VL_ATTR_ALIGNED(64);

#endif  /*guard*/
