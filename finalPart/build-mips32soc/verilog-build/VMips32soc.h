// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Primary design header
//
// This header should be included by all source files instantiating the design.
// The class here is then constructed to instantiate the design.
// See the Verilator manual for examples.

#ifndef _VMips32soc_H_
#define _VMips32soc_H_

#include "verilated.h"
#include "VMips32soc__Dpi.h"

class VMips32soc__Syms;
class VMips32soc_Mips32soc;

//----------

VL_MODULE(VMips32soc) {
  public:
    // CELLS
    // Public to allow access to /*verilator_public*/ items;
    // otherwise the application code can consider these internals.
    VMips32soc_Mips32soc*	Mips32soc;
    
    // PORTS
    // The application code writes and reads these signals to
    // propagate new values into/out from the Verilated model.
    VL_IN8(fclk,0,0);
    VL_IN8(rst,0,0);
    VL_IN8(keypad,7,0);
    VL_OUT8(green,2,0);
    VL_OUT8(blue,1,0);
    VL_OUT8(hsync,0,0);
    VL_OUT8(vsync,0,0);
    VL_OUT8(red,2,0);
    VL_OUT8(iAddr,0,0);
    VL_OUT8(iOpc,0,0);
    VL_OUT8(iPC,0,0);
    VL_OUT8(err,0,0);
    
    // LOCAL SIGNALS
    // Internals; generally not touched by application code
    
    // LOCAL VARIABLES
    // Internals; generally not touched by application code
    VL_SIG8(__VinpClk__TOP__Mips32soc____PVT__cclk,0,0);
    VL_SIG8(__Vclklast__TOP__fclk,0,0);
    VL_SIG8(__Vclklast__TOP____VinpClk__TOP__Mips32soc____PVT__cclk,0,0);
    VL_SIG8(__Vchglast__TOP__Mips32soc__cclk,0,0);
    
    // INTERNAL VARIABLES
    // Internals; generally not touched by application code
    //char	__VpadToAlign28[4];
    VMips32soc__Syms*	__VlSymsp;		// Symbol table
    
    // PARAMETERS
    // Parameters marked /*verilator public*/ for use by application code
    
    // CONSTRUCTORS
  private:
    VMips32soc& operator= (const VMips32soc&);	///< Copying not allowed
    VMips32soc(const VMips32soc&);	///< Copying not allowed
  public:
    /// Construct the model; called by application code
    /// The special name  may be used to make a wrapper with a
    /// single model invisible WRT DPI scope names.
    VMips32soc(const char* name="TOP");
    /// Destroy the model; called (often implicitly) by application code
    ~VMips32soc();
    
    // USER METHODS
    
    // API METHODS
    /// Evaluate the model.  Application must call when inputs change.
    void eval();
    /// Simulation complete, run final blocks.  Application must call on completion.
    void final();
    
    // INTERNAL METHODS
  private:
    static void _eval_initial_loop(VMips32soc__Syms* __restrict vlSymsp);
  public:
    void __Vconfigure(VMips32soc__Syms* symsp, bool first);
  private:
    static QData	_change_request(VMips32soc__Syms* __restrict vlSymsp);
  public:
    static void	_combo__TOP__8(VMips32soc__Syms* __restrict vlSymsp);
  private:
    void	_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first);
    void	_ctor_var_reset();
  public:
    static void	_eval(VMips32soc__Syms* __restrict vlSymsp);
    static void	_eval_initial(VMips32soc__Syms* __restrict vlSymsp);
    static void	_eval_settle(VMips32soc__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__2(VMips32soc__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__6(VMips32soc__Syms* __restrict vlSymsp);
    static void	_settle__TOP__1(VMips32soc__Syms* __restrict vlSymsp);
    static void	_settle__TOP__10(VMips32soc__Syms* __restrict vlSymsp);
    static void	_settle__TOP__3(VMips32soc__Syms* __restrict vlSymsp);
    static void	_settle__TOP__4(VMips32soc__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
