// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VMips32soc.h for the primary calling header

#include "VMips32soc_RegFile.h" // For This
#include "VMips32soc__Syms.h"

#include "verilated_dpi.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VMips32soc_RegFile) {
    // Reset internal values
    // Reset structure values
    _ctor_var_reset();
}

void VMips32soc_RegFile::__Vconfigure(VMips32soc__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VMips32soc_RegFile::~VMips32soc_RegFile() {
}

//--------------------
// Internal Methods

void VMips32soc_RegFile::_initial__TOP__Mips32soc__regFile__1(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("        VMips32soc_RegFile::_initial__TOP__Mips32soc__regFile__1\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // INITIAL at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:129
    vlSymsp->TOP__Mips32soc__regFile.memory[0U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[1U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[2U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[3U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[4U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[5U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[6U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[7U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[8U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[9U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0xaU] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0xbU] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0xcU] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0xdU] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0xeU] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0xfU] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0x10U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0x11U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0x12U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0x13U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0x14U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0x15U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0x16U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0x17U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0x18U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0x19U] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0x1aU] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0x1bU] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0x1cU] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0x1dU] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0x1eU] = 0U;
    vlSymsp->TOP__Mips32soc__regFile.memory[0x1fU] = 0U;
}

VL_INLINE_OPT void VMips32soc_RegFile::_sequent__TOP__Mips32soc__regFile__2(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("        VMips32soc_RegFile::_sequent__TOP__Mips32soc__regFile__2\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlSymsp->TOP__Mips32soc__regFile.__Vdlyvset__memory__v0 = 0U;
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:136
    if (vlSymsp->TOP__Mips32soc.__PVT__regWrite) {
	vlSymsp->TOP__Mips32soc__regFile.__Vdlyvval__memory__v0 
	    = ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__memtoReg))
	        ? ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__memtoReg))
		    ? ((IData)(4U) + vlSymsp->TOP__Mips32soc.__PVT__DIG_Register_BUS_i3__DOT__state)
		    : vlSymsp->TOP__Mips32soc.__PVT__s15)
	        : ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__memtoReg))
		    ? ((0U == (IData)(vlSymsp->TOP__Mips32soc.__PVT__mds))
		        ? vlSymsp->TOP__Mips32soc.__PVT__s19
		        : ((1U == (IData)(vlSymsp->TOP__Mips32soc.__PVT__mds))
			    ? ((1U >= (3U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__s16)))
			        ? ((0xffff0000U & (
						   ((IData)(vlSymsp->TOP__Mips32soc.__PVT__szel)
						     ? 
						    VL_NEGATE_I((IData)(
									(1U 
									 & (vlSymsp->TOP__Mips32soc.__PVT__s19 
									    >> 0x1fU))))
						     : 0U) 
						   << 0x10U)) 
				   | (0xffffU & (vlSymsp->TOP__Mips32soc.__PVT__s19 
						 >> 0x10U)))
			        : ((0xffff0000U & (
						   ((IData)(vlSymsp->TOP__Mips32soc.__PVT__szel)
						     ? 
						    VL_NEGATE_I((IData)(
									(1U 
									 & (vlSymsp->TOP__Mips32soc.__PVT__s19 
									    >> 0xfU))))
						     : 0U) 
						   << 0x10U)) 
				   | (0xffffU & vlSymsp->TOP__Mips32soc.__PVT__s19)))
			    : ((2U == (IData)(vlSymsp->TOP__Mips32soc.__PVT__mds))
			        ? ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__s16))
				    ? ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__s16))
				        ? ((0xffffff00U 
					    & (((IData)(vlSymsp->TOP__Mips32soc.__PVT__szel)
						 ? 
						VL_NEGATE_I((IData)(
								    (1U 
								     & (vlSymsp->TOP__Mips32soc.__PVT__s19 
									>> 7U))))
						 : 0U) 
					       << 8U)) 
					   | (0xffU 
					      & vlSymsp->TOP__Mips32soc.__PVT__s19))
				        : ((0xffffff00U 
					    & (((IData)(vlSymsp->TOP__Mips32soc.__PVT__szel)
						 ? 
						VL_NEGATE_I((IData)(
								    (1U 
								     & (vlSymsp->TOP__Mips32soc.__PVT__s19 
									>> 0xfU))))
						 : 0U) 
					       << 8U)) 
					   | (0xffU 
					      & (vlSymsp->TOP__Mips32soc.__PVT__s19 
						 >> 8U))))
				    : ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__s16))
				        ? ((0xffffff00U 
					    & (((IData)(vlSymsp->TOP__Mips32soc.__PVT__szel)
						 ? 
						VL_NEGATE_I((IData)(
								    (1U 
								     & (vlSymsp->TOP__Mips32soc.__PVT__s19 
									>> 0x17U))))
						 : 0U) 
					       << 8U)) 
					   | (0xffU 
					      & (vlSymsp->TOP__Mips32soc.__PVT__s19 
						 >> 0x10U)))
				        : ((0xffffff00U 
					    & (((IData)(vlSymsp->TOP__Mips32soc.__PVT__szel)
						 ? 
						VL_NEGATE_I((IData)(
								    (1U 
								     & (vlSymsp->TOP__Mips32soc.__PVT__s19 
									>> 0x1fU))))
						 : 0U) 
					       << 8U)) 
					   | (0xffU 
					      & (vlSymsp->TOP__Mips32soc.__PVT__s19 
						 >> 0x18U)))))
			        : 0U))) : vlSymsp->TOP__Mips32soc.aRes));
	vlSymsp->TOP__Mips32soc__regFile.__Vdlyvset__memory__v0 = 1U;
	vlSymsp->TOP__Mips32soc__regFile.__Vdlyvdim0__memory__v0 
	    = (0x1fU & ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__regDest))
			 ? ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__regDest))
			     ? 1U : 0x1fU) : ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__regDest))
					       ? (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
						  >> 0xbU)
					       : (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
						  >> 0x10U))));
    }
    // ALWAYSPOST at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:138
    if (vlSymsp->TOP__Mips32soc__regFile.__Vdlyvset__memory__v0) {
	vlSymsp->TOP__Mips32soc__regFile.memory[vlSymsp->TOP__Mips32soc__regFile.__Vdlyvdim0__memory__v0] 
	    = vlSymsp->TOP__Mips32soc__regFile.__Vdlyvval__memory__v0;
    }
}

VL_INLINE_OPT void VMips32soc_RegFile::_sequent__TOP__Mips32soc__regFile__3(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("        VMips32soc_RegFile::_sequent__TOP__Mips32soc__regFile__3\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlSymsp->TOP__Mips32soc__regFile.__PVT__Db = vlSymsp->TOP__Mips32soc__regFile.memory
	[(0x1fU & (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
		   >> 0x10U))];
    vlSymsp->TOP__Mips32soc__regFile.__PVT__Da = vlSymsp->TOP__Mips32soc__regFile.memory
	[(0x1fU & (vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
		   >> 0x15U))];
}

void VMips32soc_RegFile::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("        VMips32soc_RegFile::_ctor_var_reset\n"); );
    // Body
    __PVT__Din = VL_RAND_RESET_I(32);
    __PVT__we = VL_RAND_RESET_I(1);
    __PVT__Rw = VL_RAND_RESET_I(5);
    __PVT__C = VL_RAND_RESET_I(1);
    __PVT__Ra = VL_RAND_RESET_I(5);
    __PVT__Rb = VL_RAND_RESET_I(5);
    __PVT__Da = VL_RAND_RESET_I(32);
    __PVT__Db = VL_RAND_RESET_I(32);
    { int __Vi0=0; for (; __Vi0<32; ++__Vi0) {
	    memory[__Vi0] = VL_RAND_RESET_I(32);
    }}
    __Vdlyvdim0__memory__v0 = VL_RAND_RESET_I(5);
    __Vdlyvval__memory__v0 = VL_RAND_RESET_I(32);
    __Vdlyvset__memory__v0 = VL_RAND_RESET_I(1);
}

void VMips32soc_RegFile::_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("        VMips32soc_RegFile::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
