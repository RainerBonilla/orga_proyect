// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See VMips32soc.h for the primary calling header

#ifndef _VMips32soc_RegFile_H_
#define _VMips32soc_RegFile_H_

#include "verilated.h"
#include "VMips32soc__Dpi.h"

class VMips32soc__Syms;

//----------

VL_MODULE(VMips32soc_RegFile) {
  public:
    // CELLS
    
    // PORTS
    VL_IN8(__PVT__C,0,0);
    VL_IN8(__PVT__we,0,0);
    VL_IN8(__PVT__Rw,4,0);
    VL_IN8(__PVT__Ra,4,0);
    VL_IN8(__PVT__Rb,4,0);
    //char	__VpadToAlign5[3];
    VL_IN(__PVT__Din,31,0);
    VL_OUT(__PVT__Da,31,0);
    VL_OUT(__PVT__Db,31,0);
    
    // LOCAL SIGNALS
    VL_SIG(memory[32],31,0);
    
    // LOCAL VARIABLES
    VL_SIG8(__Vdlyvdim0__memory__v0,4,0);
    VL_SIG8(__Vdlyvset__memory__v0,0,0);
    //char	__VpadToAlign158[2];
    VL_SIG(__Vdlyvval__memory__v0,31,0);
    
    // INTERNAL VARIABLES
  private:
    VMips32soc__Syms*	__VlSymsp;		// Symbol table
  public:
    
    // PARAMETERS
    
    // CONSTRUCTORS
  private:
    VMips32soc_RegFile& operator= (const VMips32soc_RegFile&);	///< Copying not allowed
    VMips32soc_RegFile(const VMips32soc_RegFile&);	///< Copying not allowed
  public:
    VMips32soc_RegFile(const char* name="TOP");
    ~VMips32soc_RegFile();
    
    // USER METHODS
    
    // API METHODS
    
    // INTERNAL METHODS
    void __Vconfigure(VMips32soc__Syms* symsp, bool first);
  private:
    void	_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first);
    void	_ctor_var_reset();
  public:
    static void	_initial__TOP__Mips32soc__regFile__1(VMips32soc__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__Mips32soc__regFile__2(VMips32soc__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__Mips32soc__regFile__3(VMips32soc__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
