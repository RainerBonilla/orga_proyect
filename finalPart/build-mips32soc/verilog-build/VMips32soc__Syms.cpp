// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table implementation internals

#include "VMips32soc__Syms.h"
#include "VMips32soc.h"
#include "VMips32soc_Mips32soc.h"
#include "VMips32soc_RegFile.h"
#include "VMips32soc_VGATextCard.h"
#include "VMips32soc_DataMem.h"
#include "VMips32soc_InstMem.h"
#include "VMips32soc_DualPortVGARam.h"
#include "VMips32soc_FontRom.h"
#include "VMips32soc_RomColorPalette.h"

// FUNCTIONS
VMips32soc__Syms::VMips32soc__Syms(VMips32soc* topp, const char* namep)
	// Setup locals
	: __Vm_namep(namep)
	, __Vm_activity(false)
	, __Vm_didInit(false)
	// Setup submodule names
	, TOP__Mips32soc                 (Verilated::catName(topp->name(),"Mips32soc"))
	, TOP__Mips32soc__dataMem        (Verilated::catName(topp->name(),"Mips32soc.dataMem"))
	, TOP__Mips32soc__instMem        (Verilated::catName(topp->name(),"Mips32soc.instMem"))
	, TOP__Mips32soc__regFile        (Verilated::catName(topp->name(),"Mips32soc.regFile"))
	, TOP__Mips32soc__vgaTc          (Verilated::catName(topp->name(),"Mips32soc.vgaTc"))
	, TOP__Mips32soc__vgaTc__fontRom (Verilated::catName(topp->name(),"Mips32soc.vgaTc.fontRom"))
	, TOP__Mips32soc__vgaTc__frameBuff (Verilated::catName(topp->name(),"Mips32soc.vgaTc.frameBuff"))
	, TOP__Mips32soc__vgaTc__palRom  (Verilated::catName(topp->name(),"Mips32soc.vgaTc.palRom"))
{
    // Pointer to top level
    TOPp = topp;
    // Setup each module's pointers to their submodules
    TOPp->Mips32soc                 = &TOP__Mips32soc;
    TOPp->Mips32soc->dataMem        = &TOP__Mips32soc__dataMem;
    TOPp->Mips32soc->instMem        = &TOP__Mips32soc__instMem;
    TOPp->Mips32soc->regFile        = &TOP__Mips32soc__regFile;
    TOPp->Mips32soc->vgaTc          = &TOP__Mips32soc__vgaTc;
    TOPp->Mips32soc->vgaTc->fontRom  = &TOP__Mips32soc__vgaTc__fontRom;
    TOPp->Mips32soc->vgaTc->frameBuff  = &TOP__Mips32soc__vgaTc__frameBuff;
    TOPp->Mips32soc->vgaTc->palRom  = &TOP__Mips32soc__vgaTc__palRom;
    // Setup each module's pointer back to symbol table (for public functions)
    TOPp->__Vconfigure(this, true);
    TOP__Mips32soc.__Vconfigure(this, true);
    TOP__Mips32soc__dataMem.__Vconfigure(this, true);
    TOP__Mips32soc__instMem.__Vconfigure(this, true);
    TOP__Mips32soc__regFile.__Vconfigure(this, true);
    TOP__Mips32soc__vgaTc.__Vconfigure(this, true);
    TOP__Mips32soc__vgaTc__fontRom.__Vconfigure(this, true);
    TOP__Mips32soc__vgaTc__frameBuff.__Vconfigure(this, true);
    TOP__Mips32soc__vgaTc__palRom.__Vconfigure(this, true);
    // Setup scope names
    __Vscope_Mips32soc.configure(this,name(),"Mips32soc");
    __Vscope_Mips32soc__dataMem.configure(this,name(),"Mips32soc.dataMem");
    __Vscope_Mips32soc__instMem.configure(this,name(),"Mips32soc.instMem");
    __Vscope_Mips32soc__regFile.configure(this,name(),"Mips32soc.regFile");
    __Vscope_Mips32soc__vgaTc__fontRom.configure(this,name(),"Mips32soc.vgaTc.fontRom");
    __Vscope_Mips32soc__vgaTc__frameBuff.configure(this,name(),"Mips32soc.vgaTc.frameBuff");
    __Vscope_Mips32soc__vgaTc__palRom.configure(this,name(),"Mips32soc.vgaTc.palRom");
    // Setup export functions
    for (int __Vfinal=0; __Vfinal<2; __Vfinal++) {
	__Vscope_Mips32soc.varInsert(__Vfinal,"aRes", &(TOP__Mips32soc.aRes), VLVT_UINT32,VLVD_NODIR|VLVF_PUB_RW,1 ,31,0);
	__Vscope_Mips32soc.varInsert(__Vfinal,"pc", &(TOP__Mips32soc.pc), VLVT_UINT32,VLVD_NODIR|VLVF_PUB_RW,1 ,31,0);
	__Vscope_Mips32soc__dataMem.varInsert(__Vfinal,"memory", &(TOP__Mips32soc__dataMem.memory), VLVT_UINT32,VLVD_NODIR|VLVF_PUB_RW,2 ,31,0 ,2047,0);
	__Vscope_Mips32soc__instMem.varInsert(__Vfinal,"memory", &(TOP__Mips32soc__instMem.memory), VLVT_UINT32,VLVD_NODIR|VLVF_PUB_RW,2 ,31,0 ,2047,0);
	__Vscope_Mips32soc__regFile.varInsert(__Vfinal,"memory", &(TOP__Mips32soc__regFile.memory), VLVT_UINT32,VLVD_NODIR|VLVF_PUB_RW,2 ,31,0 ,31,0);
	__Vscope_Mips32soc__vgaTc__fontRom.varInsert(__Vfinal,"memory", &(TOP__Mips32soc__vgaTc__fontRom.memory), VLVT_UINT8,VLVD_NODIR|VLVF_PUB_RW,2 ,7,0 ,4095,0);
	__Vscope_Mips32soc__vgaTc__frameBuff.varInsert(__Vfinal,"memory", &(TOP__Mips32soc__vgaTc__frameBuff.memory), VLVT_UINT32,VLVD_NODIR|VLVF_PUB_RW,2 ,31,0 ,2047,0);
	__Vscope_Mips32soc__vgaTc__palRom.varInsert(__Vfinal,"memory", &(TOP__Mips32soc__vgaTc__palRom.memory), VLVT_UINT8,VLVD_NODIR|VLVF_PUB_RW,2 ,7,0 ,15,0);
    }
}
