// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See VMips32soc.h for the primary calling header

#ifndef _VMips32soc_InstMem_H_
#define _VMips32soc_InstMem_H_

#include "verilated.h"
#include "VMips32soc__Dpi.h"

class VMips32soc__Syms;

//----------

VL_MODULE(VMips32soc_InstMem) {
  public:
    // CELLS
    
    // PORTS
    VL_IN8(__PVT__clk,0,0);
    VL_IN8(__PVT__en,0,0);
    VL_IN16(__PVT__addr,10,0);
    VL_OUT(__PVT__dout,31,0);
    
    // LOCAL SIGNALS
    //char	__VpadToAlign12[4];
    VL_SIG(memory[2048],31,0);
    
    // LOCAL VARIABLES
    
    // INTERNAL VARIABLES
  private:
    VMips32soc__Syms*	__VlSymsp;		// Symbol table
  public:
    
    // PARAMETERS
    
    // CONSTRUCTORS
  private:
    VMips32soc_InstMem& operator= (const VMips32soc_InstMem&);	///< Copying not allowed
    VMips32soc_InstMem(const VMips32soc_InstMem&);	///< Copying not allowed
  public:
    VMips32soc_InstMem(const char* name="TOP");
    ~VMips32soc_InstMem();
    
    // USER METHODS
    
    // API METHODS
    
    // INTERNAL METHODS
    void __Vconfigure(VMips32soc__Syms* symsp, bool first);
  private:
    void	_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first);
    void	_ctor_var_reset();
  public:
    static void	_initial__TOP__Mips32soc__instMem__3(VMips32soc__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__Mips32soc__instMem__2(VMips32soc__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
