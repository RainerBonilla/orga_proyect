// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See VMips32soc.h for the primary calling header

#ifndef _VMips32soc_VGATextCard_H_
#define _VMips32soc_VGATextCard_H_

#include "verilated.h"
#include "VMips32soc__Dpi.h"

class VMips32soc__Syms;
class VMips32soc_RomColorPalette;
class VMips32soc_DualPortVGARam;
class VMips32soc_FontRom;

//----------

VL_MODULE(VMips32soc_VGATextCard) {
  public:
    // CELLS
    VMips32soc_RomColorPalette*	palRom;
    VMips32soc_DualPortVGARam*	frameBuff;
    VMips32soc_FontRom*	fontRom;
    
    // PORTS
    VL_IN8(__PVT__vclk,0,0);
    VL_IN8(__PVT__clk,0,0);
    VL_IN8(__PVT__rst,0,0);
    VL_IN8(__PVT__en,0,0);
    VL_IN8(__PVT__we,3,0);
    VL_OUT8(__PVT__r,2,0);
    VL_OUT8(__PVT__g,2,0);
    VL_OUT8(__PVT__b,1,0);
    VL_OUT8(__PVT__hs,0,0);
    VL_OUT8(__PVT__vs,0,0);
    VL_IN16(__PVT__addr,10,0);
    VL_IN(__PVT__wd,31,0);
    VL_OUT(__PVT__rd,31,0);
    
    // LOCAL SIGNALS
    VL_SIG8(__PVT__palrom_addr1,3,0);
    //char	__VpadToAlign25[1];
    VL_SIG16(__PVT__fontrom_addr,11,0);
    
    // LOCAL VARIABLES
    
    // INTERNAL VARIABLES
  private:
    //char	__VpadToAlign36[4];
    VMips32soc__Syms*	__VlSymsp;		// Symbol table
  public:
    
    // PARAMETERS
    
    // CONSTRUCTORS
  private:
    VMips32soc_VGATextCard& operator= (const VMips32soc_VGATextCard&);	///< Copying not allowed
    VMips32soc_VGATextCard(const VMips32soc_VGATextCard&);	///< Copying not allowed
  public:
    VMips32soc_VGATextCard(const char* name="TOP");
    ~VMips32soc_VGATextCard();
    
    // USER METHODS
    
    // API METHODS
    
    // INTERNAL METHODS
    void __Vconfigure(VMips32soc__Syms* symsp, bool first);
  private:
    void	_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first);
    void	_ctor_var_reset();
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
