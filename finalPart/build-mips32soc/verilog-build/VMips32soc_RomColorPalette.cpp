// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VMips32soc.h for the primary calling header

#include "VMips32soc_RomColorPalette.h" // For This
#include "VMips32soc__Syms.h"

#include "verilated_dpi.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VMips32soc_RomColorPalette) {
    // Reset internal values
    // Reset structure values
    _ctor_var_reset();
}

void VMips32soc_RomColorPalette::__Vconfigure(VMips32soc__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VMips32soc_RomColorPalette::~VMips32soc_RomColorPalette() {
}

//--------------------
// Internal Methods

void VMips32soc_RomColorPalette::_initial__TOP__Mips32soc__vgaTc__palRom__1(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          VMips32soc_RomColorPalette::_initial__TOP__Mips32soc__vgaTc__palRom__1\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // INITIAL at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:253
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory[0U] = 0U;
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory[1U] = 2U;
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory[2U] = 0x14U;
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory[3U] = 0x16U;
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory[4U] = 0xa0U;
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory[5U] = 0xa2U;
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory[6U] = 0xa8U;
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory[7U] = 0xb6U;
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory[8U] = 0x49U;
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory[9U] = 0x4bU;
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory[0xaU] = 0x5dU;
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory[0xbU] = 0x5fU;
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory[0xcU] = 0xe9U;
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory[0xdU] = 0xebU;
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory[0xeU] = 0xfdU;
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory[0xfU] = 0xffU;
}

void VMips32soc_RomColorPalette::_settle__TOP__Mips32soc__vgaTc__palRom__2(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          VMips32soc_RomColorPalette::_settle__TOP__Mips32soc__vgaTc__palRom__2\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlSymsp->TOP__Mips32soc__vgaTc__palRom.__PVT__color1 
	= vlSymsp->TOP__Mips32soc__vgaTc__palRom.memory
	[vlSymsp->TOP__Mips32soc__vgaTc.__PVT__palrom_addr1];
}

void VMips32soc_RomColorPalette::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("          VMips32soc_RomColorPalette::_ctor_var_reset\n"); );
    // Body
    __PVT__addr1 = VL_RAND_RESET_I(4);
    __PVT__addr2 = VL_RAND_RESET_I(4);
    __PVT__color1 = VL_RAND_RESET_I(8);
    __PVT__color2 = VL_RAND_RESET_I(8);
    { int __Vi0=0; for (; __Vi0<16; ++__Vi0) {
	    memory[__Vi0] = VL_RAND_RESET_I(8);
    }}
}

void VMips32soc_RomColorPalette::_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("          VMips32soc_RomColorPalette::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
