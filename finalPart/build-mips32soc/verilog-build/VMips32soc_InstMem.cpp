// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VMips32soc.h for the primary calling header

#include "VMips32soc_InstMem.h" // For This
#include "VMips32soc__Syms.h"

#include "verilated_dpi.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VMips32soc_InstMem) {
    // Reset internal values
    // Reset structure values
    _ctor_var_reset();
}

void VMips32soc_InstMem::__Vconfigure(VMips32soc__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VMips32soc_InstMem::~VMips32soc_InstMem() {
}

//--------------------
// Internal Methods

VL_INLINE_OPT void VMips32soc_InstMem::_sequent__TOP__Mips32soc__instMem__2(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("        VMips32soc_InstMem::_sequent__TOP__Mips32soc__instMem__2\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:518
    if ((1U & (~ (IData)(vlSymsp->TOP__Mips32soc.__PVT__iPC_temp)))) {
	vlSymsp->TOP__Mips32soc__instMem.__PVT__dout 
	    = vlSymsp->TOP__Mips32soc__instMem.memory
	    [(0x7ffU & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__s1) 
			>> 2U))];
    }
}

void VMips32soc_InstMem::_initial__TOP__Mips32soc__instMem__3(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("        VMips32soc_InstMem::_initial__TOP__Mips32soc__instMem__3\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // INITIAL at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:524
    VL_READMEM_Q (true,32,2048, 0,2, VL_ULL(0x636f64652e6d6966)
		  , vlSymsp->TOP__Mips32soc__instMem.memory
		  ,0U,0x7ffU);
}

void VMips32soc_InstMem::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("        VMips32soc_InstMem::_ctor_var_reset\n"); );
    // Body
    __PVT__clk = VL_RAND_RESET_I(1);
    __PVT__en = VL_RAND_RESET_I(1);
    __PVT__addr = VL_RAND_RESET_I(11);
    __PVT__dout = VL_RAND_RESET_I(32);
    { int __Vi0=0; for (; __Vi0<2048; ++__Vi0) {
	    memory[__Vi0] = VL_RAND_RESET_I(32);
    }}
}

void VMips32soc_InstMem::_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("        VMips32soc_InstMem::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
