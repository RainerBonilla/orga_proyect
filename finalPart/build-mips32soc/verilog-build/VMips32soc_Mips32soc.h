// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See VMips32soc.h for the primary calling header

#ifndef _VMips32soc_Mips32soc_H_
#define _VMips32soc_Mips32soc_H_

#include "verilated.h"
#include "VMips32soc__Dpi.h"

class VMips32soc__Syms;
class VMips32soc_RegFile;
class VMips32soc_VGATextCard;
class VMips32soc_DataMem;
class VMips32soc_InstMem;

//----------

VL_MODULE(VMips32soc_Mips32soc) {
  public:
    // CELLS
    VMips32soc_RegFile*	regFile;
    VMips32soc_VGATextCard*	vgaTc;
    VMips32soc_DataMem*	dataMem;
    VMips32soc_InstMem*	instMem;
    
    // PORTS
    VL_IN8(fclk,0,0);
    VL_IN8(rst,0,0);
    VL_IN8(keypad,7,0);
    VL_OUT8(green,2,0);
    VL_OUT8(blue,1,0);
    VL_OUT8(hsync,0,0);
    VL_OUT8(vsync,0,0);
    VL_OUT8(red,2,0);
    VL_OUT8(iAddr,0,0);
    VL_OUT8(iOpc,0,0);
    VL_OUT8(iPC,0,0);
    VL_OUT8(err,0,0);
    
    // LOCAL SIGNALS
    VL_SIG8(__PVT__cclk,0,0);
    VL_SIG8(__PVT__jump_taken,1,0);
    VL_SIG8(__PVT__branch_taken,0,0);
    VL_SIG8(__PVT__aluOp,3,0);
    VL_SIG8(__PVT__aluSrc,1,0);
    VL_SIG8(__PVT__regDest,1,0);
    VL_SIG8(__PVT__memtoReg,1,0);
    VL_SIG8(__PVT__jump,0,0);
    VL_SIG8(__PVT__beqs,0,0);
    VL_SIG8(__PVT__memRead,0,0);
    VL_SIG8(__PVT__memWrite,0,0);
    VL_SIG8(__PVT__regWrite,0,0);
    VL_SIG8(__PVT__bnes,0,0);
    VL_SIG8(__PVT__sze,0,0);
    VL_SIG8(__PVT__mds,1,0);
    VL_SIG8(__PVT__iOpc_temp,0,0);
    VL_SIG8(__PVT__szel,0,0);
    VL_SIG8(__PVT__jr,0,0);
    VL_SIG8(__PVT__jal,0,0);
    VL_SIG8(__PVT__blez,0,0);
    VL_SIG8(__PVT__bltz,0,0);
    VL_SIG8(__PVT__bgez,0,0);
    VL_SIG8(__PVT__bgtz,0,0);
    VL_SIG8(__PVT__shift,0,0);
    VL_SIG8(__PVT__iPC_temp,0,0);
    VL_SIG8(__PVT__iAddr_temp,0,0);
    VL_SIG8(__PVT__me,2,0);
    VL_SIG8(__PVT__mb,1,0);
    VL_SIG8(__PVT__err_temp,0,0);
    VL_SIG8(__PVT__outwe,3,0);
    VL_SIG16(__PVT__s1,12,0);
    VL_SIG16(__PVT__s16,12,0);
    //char	__VpadToAlign50[2];
    VL_SIG(pc,31,0);
    VL_SIG(__PVT__s9,31,0);
    VL_SIG(__PVT__s10,31,0);
    VL_SIG(aRes,31,0);
    VL_SIG(__PVT__sam,31,0);
    VL_SIG(__PVT__s15,31,0);
    VL_SIG(__PVT__outdata,31,0);
    VL_SIG(__PVT__s19,31,0);
    VL_SIG(__PVT__MsCounter_i2__DOT__ms_count,31,0);
    VL_SIG(__PVT__DIG_Register_BUS_i3__DOT__state,31,0);
    
    // LOCAL VARIABLES
    VL_SIG8(__Vdly__cclk,0,0);
    //char	__VpadToAlign97[3];
    
    // INTERNAL VARIABLES
  private:
    VMips32soc__Syms*	__VlSymsp;		// Symbol table
  public:
    
    // PARAMETERS
    
    // CONSTRUCTORS
  private:
    VMips32soc_Mips32soc& operator= (const VMips32soc_Mips32soc&);	///< Copying not allowed
    VMips32soc_Mips32soc(const VMips32soc_Mips32soc&);	///< Copying not allowed
  public:
    VMips32soc_Mips32soc(const char* name="TOP");
    ~VMips32soc_Mips32soc();
    
    // USER METHODS
    
    // API METHODS
    
    // INTERNAL METHODS
    void __Vconfigure(VMips32soc__Syms* symsp, bool first);
    static void	_combo__TOP__Mips32soc__13(VMips32soc__Syms* __restrict vlSymsp);
    static void	_combo__TOP__Mips32soc__15(VMips32soc__Syms* __restrict vlSymsp);
  private:
    void	_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first);
    void	_ctor_var_reset();
  public:
    static void	_initial__TOP__Mips32soc__3(VMips32soc__Syms* __restrict vlSymsp);
    static void	_initial__TOP__Mips32soc__9(VMips32soc__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__Mips32soc__10(VMips32soc__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__Mips32soc__12(VMips32soc__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__Mips32soc__2(VMips32soc__Syms* __restrict vlSymsp);
    void	_sequent__TOP__Mips32soc__4(VMips32soc__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__Mips32soc__5(VMips32soc__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__Mips32soc__6(VMips32soc__Syms* __restrict vlSymsp);
    static void	_settle__TOP__Mips32soc__1(VMips32soc__Syms* __restrict vlSymsp);
    static void	_settle__TOP__Mips32soc__11(VMips32soc__Syms* __restrict vlSymsp);
    static void	_settle__TOP__Mips32soc__14(VMips32soc__Syms* __restrict vlSymsp);
    static void	_settle__TOP__Mips32soc__16(VMips32soc__Syms* __restrict vlSymsp);
    static void	_settle__TOP__Mips32soc__7(VMips32soc__Syms* __restrict vlSymsp);
    static void	_settle__TOP__Mips32soc__8(VMips32soc__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
