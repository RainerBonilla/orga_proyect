// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VMips32soc.h for the primary calling header

#include "VMips32soc_DualPortVGARam.h" // For This
#include "VMips32soc__Syms.h"

#include "verilated_dpi.h"

//*** Below code from `systemc in Verilog file
//#line 196 "/home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v"

 #include "Mips32SocSim.h" 
//*** Above code from `systemc in Verilog file


//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(VMips32soc_DualPortVGARam) {
    // Reset internal values
    // Reset structure values
    _ctor_var_reset();
}

void VMips32soc_DualPortVGARam::__Vconfigure(VMips32soc__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VMips32soc_DualPortVGARam::~VMips32soc_DualPortVGARam() {
}

//--------------------
// Internal Methods

VL_INLINE_OPT void VMips32soc_DualPortVGARam::_sequent__TOP__Mips32soc__vgaTc__frameBuff__2(VMips32soc__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("          VMips32soc_DualPortVGARam::_sequent__TOP__Mips32soc__vgaTc__frameBuff__2\n"); );
    VMips32soc* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvset__memory__v0 = 0U;
    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvset__memory__v1 = 0U;
    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvset__memory__v2 = 0U;
    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvset__memory__v3 = 0U;
    // ALWAYS at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:202
    if ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__me))) {
	if ((8U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__outwe))) {
	    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvval__memory__v0 
		= (0xffU & vlSymsp->TOP__Mips32soc.__PVT__outdata);
	    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvset__memory__v0 = 1U;
	    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvlsb__memory__v0 = 0U;
	    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvdim0__memory__v0 
		= (0x7ffU & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__s16) 
			     >> 2U));
	}
	if ((4U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__outwe))) {
	    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvval__memory__v1 
		= (0xffU & (vlSymsp->TOP__Mips32soc.__PVT__outdata 
			    >> 8U));
	    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvset__memory__v1 = 1U;
	    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvlsb__memory__v1 = 8U;
	    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvdim0__memory__v1 
		= (0x7ffU & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__s16) 
			     >> 2U));
	}
	if ((2U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__outwe))) {
	    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvval__memory__v2 
		= (0xffU & (vlSymsp->TOP__Mips32soc.__PVT__outdata 
			    >> 0x10U));
	    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvset__memory__v2 = 1U;
	    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvlsb__memory__v2 = 0x10U;
	    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvdim0__memory__v2 
		= (0x7ffU & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__s16) 
			     >> 2U));
	}
	if ((1U & (IData)(vlSymsp->TOP__Mips32soc.__PVT__outwe))) {
	    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvval__memory__v3 
		= (0xffU & (vlSymsp->TOP__Mips32soc.__PVT__outdata 
			    >> 0x18U));
	    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvset__memory__v3 = 1U;
	    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvlsb__memory__v3 = 0x18U;
	    vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvdim0__memory__v3 
		= (0x7ffU & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__s16) 
			     >> 2U));
	}
	vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__PVT__rda 
	    = vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.memory
	    [(0x7ffU & ((IData)(vlSymsp->TOP__Mips32soc.__PVT__s16) 
			>> 2U))];
	// $c statement at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:213
updateVGADisplay();	
    }
    // ALWAYSPOST at /home/oscar/Documents/Orga/NOTES/MIPS32SOC-FinalPart/verilog/Mips32soc.v:205
    if (vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvset__memory__v0) {
	vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.memory[vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvdim0__memory__v0] 
	    = (((~ ((IData)(0xffU) << (IData)(vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvlsb__memory__v0))) 
		& vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.memory
		[vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvdim0__memory__v0]) 
	       | ((IData)(vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvval__memory__v0) 
		  << (IData)(vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvlsb__memory__v0)));
    }
    if (vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvset__memory__v1) {
	vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.memory[vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvdim0__memory__v1] 
	    = (((~ ((IData)(0xffU) << (IData)(vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvlsb__memory__v1))) 
		& vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.memory
		[vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvdim0__memory__v1]) 
	       | ((IData)(vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvval__memory__v1) 
		  << (IData)(vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvlsb__memory__v1)));
    }
    if (vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvset__memory__v2) {
	vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.memory[vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvdim0__memory__v2] 
	    = (((~ ((IData)(0xffU) << (IData)(vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvlsb__memory__v2))) 
		& vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.memory
		[vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvdim0__memory__v2]) 
	       | ((IData)(vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvval__memory__v2) 
		  << (IData)(vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvlsb__memory__v2)));
    }
    if (vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvset__memory__v3) {
	vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.memory[vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvdim0__memory__v3] 
	    = (((~ ((IData)(0xffU) << (IData)(vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvlsb__memory__v3))) 
		& vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.memory
		[vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvdim0__memory__v3]) 
	       | ((IData)(vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvval__memory__v3) 
		  << (IData)(vlSymsp->TOP__Mips32soc__vgaTc__frameBuff.__Vdlyvlsb__memory__v3)));
    }
}

void VMips32soc_DualPortVGARam::_ctor_var_reset() {
    VL_DEBUG_IF(VL_PRINTF("          VMips32soc_DualPortVGARam::_ctor_var_reset\n"); );
    // Body
    __PVT__clka = VL_RAND_RESET_I(1);
    __PVT__enablea = VL_RAND_RESET_I(1);
    __PVT__wea = VL_RAND_RESET_I(4);
    __PVT__addra = VL_RAND_RESET_I(11);
    __PVT__wda = VL_RAND_RESET_I(32);
    __PVT__rda = VL_RAND_RESET_I(32);
    __PVT__clkb = VL_RAND_RESET_I(1);
    __PVT__addrb = VL_RAND_RESET_I(11);
    __PVT__rdb = VL_RAND_RESET_I(32);
    { int __Vi0=0; for (; __Vi0<2048; ++__Vi0) {
	    memory[__Vi0] = VL_RAND_RESET_I(32);
    }}
    __Vdlyvdim0__memory__v0 = VL_RAND_RESET_I(11);
    __Vdlyvlsb__memory__v0 = VL_RAND_RESET_I(5);
    __Vdlyvval__memory__v0 = VL_RAND_RESET_I(8);
    __Vdlyvset__memory__v0 = VL_RAND_RESET_I(1);
    __Vdlyvdim0__memory__v1 = VL_RAND_RESET_I(11);
    __Vdlyvlsb__memory__v1 = VL_RAND_RESET_I(5);
    __Vdlyvval__memory__v1 = VL_RAND_RESET_I(8);
    __Vdlyvset__memory__v1 = VL_RAND_RESET_I(1);
    __Vdlyvdim0__memory__v2 = VL_RAND_RESET_I(11);
    __Vdlyvlsb__memory__v2 = VL_RAND_RESET_I(5);
    __Vdlyvval__memory__v2 = VL_RAND_RESET_I(8);
    __Vdlyvset__memory__v2 = VL_RAND_RESET_I(1);
    __Vdlyvdim0__memory__v3 = VL_RAND_RESET_I(11);
    __Vdlyvlsb__memory__v3 = VL_RAND_RESET_I(5);
    __Vdlyvval__memory__v3 = VL_RAND_RESET_I(8);
    __Vdlyvset__memory__v3 = VL_RAND_RESET_I(1);
}

void VMips32soc_DualPortVGARam::_configure_coverage(VMips32soc__Syms* __restrict vlSymsp, bool first) {
    VL_DEBUG_IF(VL_PRINTF("          VMips32soc_DualPortVGARam::_configure_coverage\n"); );
    // Body
    if (0 && vlSymsp && first) {} // Prevent unused
}
